resumeAudioState = function()
{
	CsoundObj.CSOUND_AUDIO_CONTEXT.resume();
}

suspendAudioState = function()
{
    CsoundObj.CSOUND_AUDIO_CONTEXT.suspend();
}

csoundInitialize = function()
{
    console.log("csoundInitialize");
    return CsoundObj.initialize();
}

function resumeAudioState()
{
    CsoundObj.CSOUND_AUDIO_CONTEXT.resume();
}

function suspendAudioState()
{
    CsoundObj.CSOUND_AUDIO_CONTEXT.suspend();
}

function csoundInitialize()
{
    return CsoundObj.initialize();
}

class CsoundJsDartWrapper extends CsoundObj
{
	constructor() {
		super();
        	console.log("hello from JS");
	}

	setControlChannelCallbackJs(channelName, callback) {
		let f = ()  => {
			var v = super.getControlChannel(channelName);
			callback(v);
			super.requestControlChannel(channelName);
		}
		super.requestControlChannel(channelName, f);
	}

	setStringChannelCallbackJs(channelName, callback) {
		let f = () => {
			var v = super.getStringChannel(channelName);
			callback(v);
			super.requestStringChannel(channelName);
		}
		super.requestStringChannel(channelName, f);
	}

	setTableCallbackJs(table, callback) {
		let f = () => {
			var v = super.getTable(table);
			callback(v);
			super.requestTable(table);
		}
		super.requestTable(table, f);
	}
}

CsoundJsDart = CsoundJsDartWrapper;

function createCsound()
{
	console.log("create Csound in JS");
	return new CsoundJsDartWrapper();
}
