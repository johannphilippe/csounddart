import 'dart:io';

import 'package:csounddart/csound.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:csounddart/csounddart.dart';
import 'package:oscilloscope/oscilloscope.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';


  void requestPermission() async
  {
    if(Platform.isAndroid) {
      Map<Permission, PermissionStatus> lst = await [Permission.storage,
        Permission.microphone, Permission.sensors].request();
    }
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
    requestPermission();
  }

  late Csound _cs;

  static const String _csd = """
<CsoundSynthesizer>
<CsOptions>
-odac
;-iadc
;-B 1024
;-b 256
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	32
nchnls	=	2
nchnls_i = 1
0dbfs	=	1

opcode mtofi,i,i
        imid xin
        iout = 440 * exp(0.0577622645 * (imid - 69))
        xout iout
endop


instr 1	
        inote init p4
        ivel init p5
        ifq = mtofi(inote)
        kfq = k(ifq)
        iT = 1000 / ifq

        aPulse = linseg:a(1, iT/1000, 0)
                aPulse = (aPulse + (noise:a(1,0.5) * aPulse)) / 2

                aPulse = aPulse *  pow(2, ((1-((ivel-1)/126)) * -4 ))
                iff = ivel * 100 + 20
                aPulse = butterlp(aPulse, iff)
  

        ;resonr allows to  clear internal space
                alow, ahigh, ao svfilter aPulse, kfq, 500
  
                //ao = butterbp(aPulse, kfq, 1500)
                amo = ao * 15
                amo -= 3.25
 
                amo = butterlp(amo, 150)
        //ao = lowpass2:a(amo, 150, 50) 
                amo = distort1( amo, 1, 1, 0, 0)
                ; Two parallel lines 
                ;one raising, removing gain, and DC
                a1 = (pow(amo, 2)) * (-0.08)
                a1 = dcblock(a1)

  
                        
                a2 = butterhp(amo, 250) * 0.25
  
                as = limit( ((a1 * 0.0001) + a2 + ao) * 0.7  , -1, 1)
                as = dcblock(as)


        outs as, as


endin

instr 2 
  ao = oscili(0.3, linseg:k(100, p3, 400))
  
  av = vco2(0.3, linseg:k(1, p3, 10))
  chnset(av, "vco")
  ao4 = oscili(1.0, linseg:k(1000, p3, 10))
  chnset(ao4, "sine")
  outs ao,ao
  
endin

instr 3
  ;kfq init 200
  kfq = chnget:k("freq")
  printk 1, kfq
  ao = vco2(0.15, kfq) * 0.1
  chnset(ao, "audioChannel")
  ko = k(ao)
  chnset(ko, "controlChannel")
  
  ;ain = inch(1)
  ;aVoice = ain * 0.3
  ;aVoice = delay(ain, 1) * 0.2
  ,aVoice = comb(aVoice, 3.5, 0.1) * 0.1 + (ain * 0.3)
 ; outs aVoice, aVoice
  
 outs ao,ao
endin


instr 4 
  ao = oscili(0.15, 400)
   outs ao,ao
endin
</CsInstruments>
; ==============================================
<CsScore>
i 3 0 100
</CsScore>
</CsoundSynthesizer>


  """;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    print("Init Platform State");
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion =
          await Csounddart.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  double _sliderFreq = 200;
  List<double> _dataSet = [];

  bool _pause = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Csound Dart Example'),
        ),
        body: Center(
          child: Container(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text('Running on: $_platformVersion\n'),
                  Container(
                    height: 3,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        _cs.reset();
                      },
                      child: Text("Reset")),
                  Container(
                    height: 3,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        _cs = Csound(onReady: () {
                          print("Csound Compile");
                          //print(_csd);
                          int res = _cs.compileCsdTextSync(_csd);
                          print("compile res $res");
                          //int ksmps = await _cs.getKsmps();
                          //print("ksmps = $ksmps");
                          if (res != 0) return;
                          _cs.perform();
                          _cs.setControlChannel("freq", 200.0);

                          /*
                        _cs.setControlChannelCallback("controlChannel", (double val) {
                          if (_dataSet.length > 1024) _dataSet.clear();
                         _dataSet.add(val);
                        });
                        */
                          /*
                          _cs.setAudioChannelCallback("audioChannel", (data) async {
                            if (_dataSet.length > 1024) _dataSet.clear();
                            for (int i = 0; i < data.length; i++) {
                              _dataSet.add(data.get(i));
                            }
                          });
                           */
                        },
                        onStop: (){
                          print("csound stopped callback");
                        }
                        );
                      },
                      child: Text("Play")),
                  Container(
                    height: 3,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        _cs.stop();
                        print("Want to stop");
                      },
                      child: Text("Stop")),
                  Container(height: 5),
                  ElevatedButton(
                      onPressed: () async {
                        _cs.destroy(onDestroyed: (){
                          print("Csound Destroy callback");
                        });
                      },
                      child: Text("destroy")),
                  Container(height: 5),
                  ElevatedButton(
                      onPressed: () async {
                        _pause = !_pause;
                        _cs.pause(_pause);
                      },
                      child: Text("pause")),
                  Container(height: 5),
                  Slider(
                    onChanged: (double v) {
                      setState(() {
                        _sliderFreq = v;
                      });
                      _cs.setControlChannel("freq", _sliderFreq);
                    },
                    value: _sliderFreq,
                    min: 100,
                    max: 500,
                  ),
                  Container(
                    width: 400,
                    height: 150,
                    child: Oscilloscope(
                      showYAxis: true,
                      yAxisColor: Colors.orange,
                      margin: EdgeInsets.all(20.0),
                      strokeWidth: 1.0,
                      backgroundColor: Colors.black,
                      traceColor: Colors.green,
                      yAxisMax: 1.0,
                      yAxisMin: -1.0,
                      dataSet: _dataSet,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
