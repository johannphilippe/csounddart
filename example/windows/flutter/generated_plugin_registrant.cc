//
//  Generated file. Do not edit.
//

#include "generated_plugin_registrant.h"

#include <csounddart/csounddart_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  CsounddartPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("CsounddartPlugin"));
}
