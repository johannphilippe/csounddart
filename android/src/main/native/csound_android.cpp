//
// Created by johann on 13/08/21.
// The idea of this class is to generate a wrapper class around Csound and Oboe to create audio output callbacks when starting Csound.
// This class will be called by the implementation of CsoundDart.
// It is inspired by Michael Gogins implementation [csound_oboe.hpp](https://github.com/gogins/csound-android/blob/master/CsoundForAndroid/CsoundAndroid/jni/csound_oboe.hpp)
//

#include<iostream>
#include"include/csound.h"
#include<oboe/Oboe.h>
#include<memory>
#include <android/log.h>
#include<thread>
#include<sstream>
#include"CallbackManager.h"
#include<atomic>
#include<queue>

template<typename Data>
class concurrent_queue
{
private:
    std::queue<Data> queue_;
    std::mutex mutex_;
    std::condition_variable condition_variable_;
public:
    void push(Data const& data)
    {

        std::unique_lock<std::mutex> lock(mutex_);
            queue_.push(data);
            lock.unlock();
            condition_variable_.notify_one();
    }
    bool empty()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return queue_.empty();
    }
    bool try_pop(Data& popped_value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if (queue_.empty()) {
            lock.unlock();
            return false;
        }
            popped_value = queue_.front();
            queue_.pop();
            lock.unlock();
            return true;
    }

    void wait_and_pop(Data& popped_value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        while (queue_.empty()) {
            condition_variable_.wait(lock);
        }
        popped_value = queue_.front();
        queue_.pop();
    }

    void notify() {
        condition_variable_.notify_all();
    }

    std::size_t size() {
        std::unique_lock<std::mutex> lock(mutex_);
        int s=queue_.size();
        condition_variable_.notify_all();
        return s;
    }
};
extern "C"
{
#define APPNAME "csounddart_example"

__attribute__((visibility("default"))) __attribute__((used))
void printC(const char *to_print)
{
    //__android_log_print(ANDROID_LOG_DEBUG, APPNAME, "CsoundAndroid CPP -> %s", to_print);
}







/*
* CsoundAndroid - Csound C++ API with Oboe for Android
*/

class CsoundAndroid : public oboe::AudioStreamCallback
{
public:
    CsoundAndroid() : isPlaying(false), timeout_nanoseconds(1000000)

    {
        _cs = csoundCreate(NULL);
    }

    ~CsoundAndroid()
    {
        csoundDestroy(_cs);
    }

    int CompileCsdText(const char *csd_text)
    {
        return csoundCompileCsdText(_cs, csd_text);
    }

    int CompileCsd(const char *csd_path)
    {
        return csoundCompileCsd(_cs, csd_path);
    }

    int CompileOrc(const char *orchestra)
    {
        return csoundCompileOrc(_cs, orchestra);
    }

    int ReadScore(const char *score)
    {
        return csoundReadScore(_cs, score);
    }

     oboe::DataCallbackResult onAudioReady(oboe::AudioStream *oboeStream,
                void *audio_data,
                int32_t frame_count)
     {

        int res = 0;
        int ksmps = GetKsmps();
        int nchnls = GetNchnls();

        if(isPlaying == false) {
           return oboe::DataCallbackResult::Stop;
        }

        if(oboeStream->getDirection() == oboe::Direction::Input)
        {
            if( (i_nchnls > 0) && input_stream.get())
            {
                    float *float_buffer = static_cast<float *>(audio_data);
                    for(int i = 0; i < ksmps; i++)
                    {
                        for(int j = 0; j < i_nchnls; j++)
                        {
                            //spin[i * i_nchnls + j] = float_buffer[i * i_nchnls + j] * Get0dBFS();
                            queue.push(float_buffer[i * i_nchnls + j] * Get0dBFS());
                        }
                    }
            }

        } else {
            MYFLT *spout = csoundGetSpout(_cs);
            float *float_buffer = static_cast<float *>(audio_data);

            int i_nchnls = GetNchnlsInput();

            for (int i = 0; i < ksmps; i++) {

                if(input_stream.get()) {
                    for(int j = 0; j < i_nchnls; j++) {
                        float sample = 0;
                        queue.try_pop(sample);
                        spin[ i * i_nchnls + j] = sample;
                    }
                }

                for (int j = 0; j < nchnls; j++) {
                    float_buffer[i * nchnls + j] = spout[i * nchnls + j];
                }
             }
            res = PerformKsmps();
            if(send_port != -1) {
                callbackToDartInt32(send_port, res);
            }
        }
        return oboe::DataCallbackResult::Continue;
     }

    int Start()
    {
        printC("Starting");
        csoundSetHostImplementedAudioIO(_cs, 1, 0);
        int res =  csoundStart(_cs);

        isPlaying = true;

        oboe::AudioStreamBuilder builder;

        i_nchnls = GetNchnlsInput();
        if(i_nchnls > 0) {
            spin = csoundGetSpin(_cs);
            //builder.setDataCallback(stabilized_callback);
            builder.setCallback(this);
            builder.setSharingMode(oboe::SharingMode::Exclusive);
            builder.setPerformanceMode(oboe::PerformanceMode::LowLatency);
            builder.setSampleRate(GetSr());
            builder.setFramesPerCallback(GetKsmps());
            builder.setFormat(oboe::AudioFormat::Float);
            builder.setDirection(oboe::Direction::Input);
            builder.setChannelCount(i_nchnls);
            oboe::Result res = builder.openStream(input_stream);
            std::string format = oboe::convertToText(input_stream->getFormat());
             __android_log_print(ANDROID_LOG_DEBUG, APPNAME, "CsoundAndroid CPP -> input format : %s //", format.c_str());

            if(res != oboe::Result::OK) {
                __android_log_print(ANDROID_LOG_DEBUG, APPNAME, "CsoundAndroid CPP -> %s //", "cannot open input stream");
                return -1;
            }
        }

        builder.setCallback(this);
        //builder.setDataCallback(stabilized_callback);
        builder.setSharingMode(oboe::SharingMode::Exclusive);
        //builder.setPerformanceMode(oboe::PerformanceMode::LowLatency);
        builder.setSampleRate(GetSr());
        builder.setFramesPerCallback(GetKsmps());
        builder.setFormat(oboe::AudioFormat::Float);
        builder.setDirection(oboe::Direction::Output);
        builder.setChannelCount(GetNchnls());

        oboe::Result result = builder.openStream(audio_stream);
        std::thread t([=](){
            if(input_stream.get()) {
                input_stream->start();
            }
            audio_stream->requestStart();
        });
        t.detach();
        return res;
    }

    int PerformKsmps()
    {
        return csoundPerformKsmps(_cs);
    }


    int GetNchnls()
    {
        return csoundGetNchnls(_cs);
    }

    int GetNchnlsInput()
    {
        return csoundGetNchnlsInput(_cs);
    }

    int GetKsmps()
    {
        return csoundGetKsmps(_cs);
    }

    double GetSr()
    {
        return csoundGetSr(_cs);
    }

    double Get0dBFS()
    {
        return csoundGet0dBFS(_cs);
    }

    void Stop()
    {
        if(input_stream)
        {
            input_stream->requestStop();
        }
        if(audio_stream)
        {
            audio_stream->requestStop();
        }
        isPlaying = false;
        csoundStop(_cs);
    }

    void SetControlChannel(const char *channelName, double value)
    {
        csoundSetControlChannel(_cs, channelName, value);
    }

    double GetControlChannel(const char *channelName)
    {
        return csoundGetControlChannel(_cs, channelName, NULL);
    }

    void GetAudioChannel(const char *name, float *audio)
    {
        //MYFLT *float_buffer = static_cast<MYFLT *>(audio);
        csoundGetAudioChannel(_cs, name, audio);
    }

    void GetStringChannel(const char *channelName, char *value)
    {
        csoundGetStringChannel(_cs, channelName, value);
    }

    int SetOption(const char *opt)
    {
        return csoundSetOption(_cs, opt);
    }

    void Pause(bool pause)
    {
        if(audio_stream) {
            if(pause)
            {
                audio_stream->requestPause();
            } else
            {
                audio_stream->requestStart();
            }
        }
    }

    double EvalCode(const char *code)
    {
        return csoundEvalCode(_cs, code);
    }


    Dart_Port send_port = -1;
private:

    int timeout_nanoseconds;
    int i_nchnls;
    MYFLT *spin;
    std::atomic<bool> isPlaying;
    CSOUND *_cs;
    oboe::ManagedStream managedStream;
    std::shared_ptr<oboe::AudioStream> audio_stream;
    std::shared_ptr<oboe::AudioStream> input_stream;
    concurrent_queue<float> queue;
    oboe::StabilizedCallback *stabilized_callback = nullptr;
};



// C implementation for the C++ class CsoundAndroid
__attribute__((visibility("default"))) __attribute__((used))
CsoundAndroid *csoundAndroidCreate()
{
    printC("csound Create");
    return new CsoundAndroid();
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidCompileCsdText(CsoundAndroid *cs, const char *csd_text)
{
    printC("compile Csd Text");
    return cs->CompileCsdText(csd_text);
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidStart(CsoundAndroid *cs)
{
    printC("csound Start");
    return cs->Start();
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidPerformKsmps(CsoundAndroid *cs)
{
    return cs->PerformKsmps();
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidDestroy(CsoundAndroid *cs)
{
    delete cs;
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidStop(CsoundAndroid *cs)
{
    cs->Stop();
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidSetControlChannel(CsoundAndroid *cs, const char *channelName, double value)
{
    cs->SetControlChannel(channelName, value);
}
__attribute__((visibility("default"))) __attribute__((used))
double csoundAndroidGetControlChannel(CsoundAndroid *cs, const char *channelName)
{
    return cs->GetControlChannel(channelName);
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidGetAudioChannel(CsoundAndroid *cs, const char *name, float *data)
{
    cs->GetAudioChannel(name, data);
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidCompileCsd(CsoundAndroid *cs, const char *csd)
{
    return cs->CompileCsd(csd);
}
__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidCompileOrc(CsoundAndroid *cs, const char *orc)
{
    return cs->CompileOrc(orc);
}
__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidReadScore(CsoundAndroid *cs, const char *score)
{
    return cs->ReadScore(score);
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidSetCallbackPort(CsoundAndroid * cs, int64_t dart_port)
{
    std::string s = "setting dart port Cside " + std::to_string(dart_port);
    printC(s.c_str());
    cs->send_port = dart_port;
    printC("dart port set ok ");
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidGetKsmps(CsoundAndroid *cs)
{
    return cs->GetKsmps();
}
__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidGetNchnls(CsoundAndroid *cs)
{
    return cs->GetNchnls();
}
__attribute__((visibility("default"))) __attribute__((used))
double csoundAndroidGetSr(CsoundAndroid *cs)
{
    return cs->GetSr();
}
__attribute__((visibility("default"))) __attribute__((used))
double csoundAndroidGet0dBFS(CsoundAndroid *cs)
{
    return cs->Get0dBFS();
}

__attribute__((visibility("default"))) __attribute__((used))
int csoundAndroidSetOption(CsoundAndroid *cs, const char *opt)
{
    return cs->SetOption(opt);
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidPause(CsoundAndroid *cs, int pause)
{
    cs->Pause(pause == 1);
}

__attribute__((visibility("default"))) __attribute__((used))
double csoundAndroidEvalCode(CsoundAndroid *cs, const char *code)
{
   return cs->EvalCode(code);
}

__attribute__((visibility("default"))) __attribute__((used))
void csoundAndroidGetStringChannel(CsoundAndroid *cs, const char *channelName,  char *value)
{
    cs->GetStringChannel(channelName, value);
}
}
