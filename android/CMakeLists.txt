cmake_minimum_required(VERSION 3.10.1)  # for example

set(ARCHIVE_NAME "libcsound_dart.zip")
set(CSOUND_LIB_PATH "${CMAKE_CURRENT_SOURCE_DIR}/csound_lib/libcsound_dart.zip")
set(CSOUND_DOWNLOAD_URL "https://framagit.org/johannphilippe/libcsound_dart/-/archive/master/libcsound_dart-master.zip")

set(LIBCPP "https://framagit.org/johannphilippe/libcsound_dart/-/raw/master/${ANDROID_ABI}/libc++_shared.so?inline=false")
set(LIBCSOUND "https://framagit.org/johannphilippe/libcsound_dart/-/raw/master/${ANDROID_ABI}/libcsoundandroid.so?inline=false")
set(LIBSNDFILE "https://framagit.org/johannphilippe/libcsound_dart/-/raw/master/${ANDROID_ABI}/libsndfile.so?inline=false")

set(PLATFORM_LIB_PATH "${CMAKE_CURRENT_SOURCE_DIR}/src/main/cmakeLibs/${ANDROID_ABI}")

set(CPP_NAME "libc++_shared.so")
set(CSOUND_NAME "libcsoundandroid.so")
set(SNDFILE_NAME "libsndfile.so")

file(DOWNLOAD ${LIBCPP} "${PLATFORM_LIB_PATH}/${CPP_NAME}")
file(DOWNLOAD ${LIBCSOUND} "${PLATFORM_LIB_PATH}/${CSOUND_NAME}")
file(DOWNLOAD ${LIBSNDFILE} "${PLATFORM_LIB_PATH}/${SNDFILE_NAME}")

add_library(csoundandroid SHARED IMPORTED)
set_target_properties(
                       csoundandroid
                       PROPERTIES IMPORTED_LOCATION
                       ${CMAKE_CURRENT_SOURCE_DIR}/src/main/cmakeLibs/${ANDROID_ABI}/${CSOUND_NAME})

add_library(sndfile SHARED IMPORTED)
set_target_properties(
                       sndfile
                       PROPERTIES IMPORTED_LOCATION
                       ${CMAKE_CURRENT_SOURCE_DIR}/src/main/cmakeLibs/${ANDROID_ABI}/${SNDFILE_NAME})


add_library(cpp SHARED IMPORTED)
set_target_properties(
                       cpp
                       PROPERTIES IMPORTED_LOCATION
                       ${CMAKE_CURRENT_SOURCE_DIR}/src/main/cmakeLibs/${ANDROID_ABI}/${CPP_NAME})


add_library(native_callback SHARED src/main/native/CallbackManager.cpp)

if(${ANDROID_ABI} MATCHES "armeabi-v7a")
    find_package(oboe REQUIRED CONFIG)
    add_library(audio_utilities SHARED src/main/native/ConcurrentBuffer.cpp)
    add_library(csound_android SHARED "src/main/native/csound_android.cpp")
    target_link_libraries(csound_android oboe::oboe csoundandroid sndfile cpp log native_callback) # You may have other libraries here such as `log`.
elseif(${ANDROID_ABI} MATCHES "arm64-v8a")
    find_package(oboe REQUIRED CONFIG)
    add_library(audio_utilities SHARED src/main/native/ConcurrentBuffer.cpp)
    add_library(csound_android SHARED "src/main/native/csound_android.cpp")
    target_link_libraries(csound_android oboe::oboe csoundandroid sndfile cpp log native_callback) # You may have other libraries here such as `log`.
endif()
