## 0.0.1

First version. Stable on Linux.

## 0.0.2

Added reset on stop call. Also started a smarter Csound native prototype (experimental).

## 0.0.3

Made a few methods async, and fixed the Csound Control Channel methods.

## 0.0.4

Added Windows support (experimental).

## 0.0.5

Stable on Windows.

## 0.0.6

Stable on Android, Windows and Linux.
Also added the pause method, to pause performance.

## 0.0.7 

README fixed.

## 0.0.8

Android support added to pubspec.

## 0.0.9

Experimental on MacOS. Changed the lock model for concurrentbuffer. 

## 0.0.10

Added dynamic linking to MacOS in podspec.

## 0.0.11

Updated README.

## 0.0.12

- Desktop : The Csound instance pointer is now created in main isolate and immediatly passed to the worker
- Added synchronous versions for getters and compile methods
- Fixed get0dbfs and getSr (now return double)
- Add onReady, onStop and onDestroy callbacks to constructor and destructor
- Added Android audio input

## 0.0.13

- Added Android SDK support (16 and later)