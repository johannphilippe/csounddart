//
// Created by johann on 05/08/21.
//

#include<iostream>
#include<memory.h>
#include<mutex>
#include<stdlib.h>
#include<condition_variable>
#include"ConcurrentBuffer.h"
extern "C"
{

    ConcurrentBuffer::ConcurrentBuffer(size_t sizeOfBuffer) : size(sizeOfBuffer) {
        buffer = (double *)::calloc(size, sizeof(double));
    }

    void ConcurrentBuffer::set(int index, double v) {
        if(index >= size) return;
        std::unique_lock<std::mutex> lock(mutex_);
        buffer[index] = v;
        lock.unlock();
        condition_variable_.notify_one();
    }

    double ConcurrentBuffer::get(int index)
    {
        if(index >= size) {
            throw("index out of bounds");
        //return -1;
        }
        std::unique_lock<std::mutex> lock(mutex_);
        double value = buffer[index];
        lock.unlock();
        condition_variable_.notify_one();
        return value;
    }

    double *ConcurrentBuffer::getBuffer()
    {
        lock_ = std::unique_lock<std::mutex>(mutex_);
        return buffer;
    }

    void ConcurrentBuffer::unlock()
    {
       lock_.unlock();
        condition_variable_.notify_one();
    }

    void ConcurrentBuffer::free()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        delete[] buffer;
        lock.unlock();
        condition_variable_.notify_one();
    }


ConcurrentBuffer *create_concurrent_buffer(int size)
{
    return new ConcurrentBuffer(size);
}

void buffer_set(ConcurrentBuffer *buf, int index, double value)
{
    buf->set(index, value);
}

double buffer_get_value(ConcurrentBuffer *buf, int index)
{
    return buf->get(index);
}

double *buffer_get(ConcurrentBuffer *buf)
{
    return buf->getBuffer();
}

void buffer_unlock(ConcurrentBuffer *buf)
{
    buf->unlock();
}

void destroy_concurrent_buffer(ConcurrentBuffer *buf)
{
    buf->free();
    delete buf;
}

int buffer_get_size(ConcurrentBuffer *buf)
{
    return (int) buf->size;
}

}
