#include<iostream>
#include<memory.h>
#include<mutex>
#include<stdlib.h>
#include<condition_variable>

extern "C" 
{
class ConcurrentBuffer
{
public:
    ConcurrentBuffer(size_t sizeOfBuffer);

    void set(int index, double v);

    double get(int index);

    double *getBuffer();

    void unlock();

    void free();


    std::unique_lock<std::mutex> lock_;
    std::mutex mutex_;
    std::condition_variable condition_variable_;
    size_t size;
    double *buffer;
};

__declspec(dllexport)
ConcurrentBuffer *create_concurrent_buffer(int size);
__declspec(dllexport)
void buffer_set(ConcurrentBuffer *buf, int index, double value);
__declspec(dllexport)
double buffer_get_value(ConcurrentBuffer *buf, int index);
__declspec(dllexport)
double *buffer_get(ConcurrentBuffer *buf);
__declspec(dllexport)
void buffer_unlock(ConcurrentBuffer *buf);
__declspec(dllexport)
void destroy_concurrent_buffer(ConcurrentBuffer *buf);
__declspec(dllexport)
int buffer_get_size(ConcurrentBuffer *buf);
}