cmake_minimum_required(VERSION 3.15)
set(PROJECT_NAME "csounddart")
project(${PROJECT_NAME} LANGUAGES CXX)

# This value is used when generating builds using this plugin, so it must
# not be changed
set(PLUGIN_NAME "csounddart_plugin")

add_library(${PLUGIN_NAME} SHARED
  "csounddart_plugin.cpp"
  "include/ConcurrentBuffer.cpp"
)

#add_library(audio_utilities SHARED "include/ConcurrentBuffer.cpp")
#apply_standard_settings(audio_utilities)
#set_target_properties(audio_utilities PROPERTIES CXX_VISIBILITY_PRESET default)
#target_compile_definitions(audio_utilities PRIVATE FLUTTER_PLUGIN_IMPL)
#target_include_directories(audio_utilities INTERFACE
#  "${CMAKE_CURRENT_SOURCE_DIR}/include")


apply_standard_settings(${PLUGIN_NAME})
set_target_properties(${PLUGIN_NAME} PROPERTIES
  CXX_VISIBILITY_PRESET hidden)
target_compile_definitions(${PLUGIN_NAME} PRIVATE FLUTTER_PLUGIN_IMPL)
target_include_directories(${PLUGIN_NAME} INTERFACE
  "${CMAKE_CURRENT_SOURCE_DIR}/include")
target_link_libraries(${PLUGIN_NAME} PRIVATE flutter flutter_wrapper_plugin)

#install(TARGETS audio_utilities DESTINATION "${CMAKE_INSTALL_PREFIX}")
# List of absolute paths to libraries that should be bundled with the plugin
set(csounddart_bundled_libraries
  ""
  PARENT_SCOPE
)
