# csounddart

An Dart interface to Csound API. The intention of this project is to provide an unified API for creating applications with Flutter and Csound - whatever the target is (Linux, MacOS, Windows, Web, Android, iOS... and probably Fushia in the future).
It is currently under development.
It has been tested on Linux, MacOS, Windows and Android.
The web version is not working currently.

On desktop platforms, it assumes Csound is already installed on your computer.
On Android it will download and install Csound library in the application bundle.

## Getting Started

``` dart
import 'package:csounddart/csound.dart';

//Create the Csound object in a new Isolate (sort of Dart thread).
//Due to current implementation, all transaction with
// Csound is done asynchronously by sending a message to the isolate.
Csound _cs = Csound();

// Compiles Csound CSD text and gets the return value
int res = await _cs.compileCsdText(_myCsdText);

// Reads a piece of score
int res = await _cs.readScore(_myScore);

// Compiles from path
int res = await _cs.compileCsd(_myCsd);

// Compiles Orchestra
int res = await _cs.compileOrc(_orchestra);

// threaded performance
_cs.perform();

// pauses or restart performance
_cs.pause(bool);

// set a channel value
_cs.setControlChannel("channelName", (double)value);

// set a callback. The passed method will be called at every k-pass.
_cs.setControlChannelCallback("channelName",
    (double v) {
        // do something with callback
    }
);

// Audio callback
_cs.setAudioChannelCallback("audioChannelName",
    (ConcurrentBuffer buf) {
        for(int i = 0; i < buf.length; i++) {
            double value = buf.get(i);
        }
    }
);

// retrieves a control channel value
double channel = _cs.getControlChannel("channemName");

// Gets values from Csound
int ksmps = await _cs.getKsmps();
int nchnls = await _cs.getNchnls();
int sr = await _cs.getSr();
int dbfs = await _cs.get0dbfs();

// Stops Csound, clears all callback memory.
_cs.stop();

// Stops performance is running, and destroys Csound instance, all callback memory, and kills isolate.
// Start a new performance involves to re-create an object by calling constructor `Csound()`.
_cs.destroy();

// Resets Csound
_cs.reset();

```

## TODO : 

- Android : Audio channel callback seems broken.
- Unify ConcurrentBuffer so that it can be easy to use from user side

