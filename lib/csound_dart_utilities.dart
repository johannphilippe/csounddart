import 'dart:ffi';
import 'package:csounddart/audio_utilities.dart';
import 'package:ffi/ffi.dart';

class Boolean {
  Boolean(this.value);
  bool value = false;
}
class CallbackType
{
  CallbackType(this.cName);
  List<int> list = [];
  Pointer<Utf8> cName;
}

class CallbackAndroidType
{
  CallbackAndroidType(this.cName);
  Pointer<Utf8> cName;
  List<Function> callbacks = [];
}

class AudioCallbackAndroidType
{
  AudioCallbackAndroidType(this.cName, int size) : buf = ConcurrentBufferAndroid(size);
  AudioCallbackAndroidType.noBuffer();

  Pointer<Utf8>? cName;
  List<Function> callbacks = [];
  ConcurrentBufferAndroid? buf;
}
