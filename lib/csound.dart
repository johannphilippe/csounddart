import 'dart:async';
import 'package:flutter/services.dart';
import "csound_stub.dart"
// ignore: uri_does_not_exist
if(dart.library.io) 'csound_native.dart'
// ignore: uri_does_not_exist
if(dart.library.html) 'csound_web.dart';

Function appStartup = getAppStartup();

abstract class Csound {
  static const MethodChannel _channel =
      const MethodChannel('csound');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  //Constructor
  factory Csound({Function? onReady, Function? onStop}) => getCsoundDart(onReady: onReady, onStop: onStop);

  //Compile the CSD string as text
  Future<int> compileCsdText(String text);
  int compileCsdTextSync(String text);

  //Compiles the ORC string as text
  Future<int> compileOrc(String orc);
  int compileOrcSync(String orc);

  //Compiles the CSD string as file
  Future<int> compileCsd(String csdPath);
  int compileCsdSync(String csdPath);

  //Reads a piece of score
  Future<int> readScore(String text);
  int readScoreSync(String text);

  // Add an option String to Csound
  Future<int> setOption(String opt);
  int setOptionSync(String opt);

  //starts performance asynchronously
  int perform();

  //stops performance
  void stop();

  // if true pause performance, else play.
  void pause(bool b);

  void reset();
  void resetSync();

  //Sets control channel "channelName" with value
  void setControlChannel(String channelName, double value);

  //Returns the value of control channel "channelName". Not working on desktop yet
  // (It is recommanded to use setControlChannelCallback instead, see below).
  Future<double> getControlChannel(String channelName);
  double getControlChannelSync(String channelName);

  //Associates the callback of a control channel with a function. Multiple callbacks can be registered for one channel. 
  // It is user responsibility to pass a function with a double value as single argument for callback argument.
  void setControlChannelCallback(String channelName, Function(double) callback);

  //Sets String channel "channelName" with string value
  void setStringChannel(String channelName, String value);

  //Returns the value of String channel "channelName". Not working yet 
  Future<String> getStringChannel(String channelName);
  String getStringChannelSync(String channelName);

  //Associates the callback of a String channel with a function. Multiple callbacks can be registered for one channel. 
  // It is user responsibility to pass a function with a String value as single argument for callback argument.
  void setStringChannelCallback(String channelName, Function callback);

  // Sets the corresponding table with values
  void setTable(int table, List<double> values);

  //Assiciates the callback of table with a function.
  void setTableCallback(int table, Function callback);

  /*
  --------------------------------------------------------------
  The following methods are only available on desktop and mobile
  --------------------------------------------------------------
   */

  void setAudioChannelCallback(String name, Function callback);

  // Evaluates a String of orc, and returns the "return" value of the orc if successful.
  Future<double> evalCode(String code);
  double evalCodeSync(String code);
  static double EvalCode(String code) => csoundEvaluateCode(code);

  //Retrieves values from Csound engine
  // Those methods might be implemented for web as well .
  Future<int> getKsmps();
  int getKsmpsSync();
  Future<int> getNchnls();
  int getNchnlsSync();
  Future<double> getSr();
  double getSrSync();
  Future<double> get0dbfs();
  double get0dbfsSync();

  void destroy({Function? onDestroyed});
}



