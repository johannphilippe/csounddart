import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:isolate';
import 'package:csounddart/csound_dart_utilities.dart';
import 'package:csounddart/audio_utilities.dart';
import 'package:csounddart/csound_native.dart';
import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'csound.dart';

DynamicLibrary getLib()
{
  if(Platform.isAndroid) {
    DynamicLibrary androLib = DynamicLibrary.open("libcsound_android.so");
    return androLib;
  }
  throw("Not Android");
}

DynamicLibrary androLib = getLib();

typedef csound_android_create_func = Pointer<Void> Function();
typedef csoundAndroidCreateFunc = Pointer<Void> Function();

typedef csound_android_compile_csd_text_func = Int32 Function(Pointer<Void> cs, Pointer<Utf8> csd);
typedef csoundAndroidCompileCsdTextfunc = int Function(Pointer<Void>, Pointer<Utf8>);

typedef csound_android_start_func = Int32 Function(Pointer<Void>);
typedef csoundAndroidStartFunc = int Function(Pointer<Void>);
final csoundAndroidGetKsmps = androLib.lookupFunction<csound_android_start_func, csoundAndroidStartFunc>("csoundAndroidGetKsmps");
final csoundAndroidGetNchnls = androLib.lookupFunction<csound_android_start_func, csoundAndroidStartFunc>("csoundAndroidGetNchnls");

typedef csound_android_get_sr_func = Double Function(Pointer<Void>);
typedef csoundAndroidGetSrFunc = double Function(Pointer<Void>);
final csoundAndroidGetSr = androLib.lookupFunction<csound_android_get_sr_func, csoundAndroidGetSrFunc>("csoundAndroidGetSr");
final csoundAndroidGet0dBFS = androLib.lookupFunction<csound_android_get_sr_func, csoundAndroidGetSrFunc>("csoundAndroidGet0dBFS");

typedef csound_android_stop_func = Void Function(Pointer<Void>);
typedef csoundAndroidStopFunc = void Function(Pointer<Void>);
final csoundAndroidDestroy = androLib.lookupFunction<csound_android_stop_func, csoundAndroidStopFunc>("csoundAndroidDestroy");

typedef csa_set_control_channel_func = Void Function(Pointer<Void>, Pointer<Utf8>, Double d);
typedef csaSetControlChannelFunc = void Function(Pointer<Void>, Pointer<Utf8>, double);
final csoundAndroidSetControlChannel = androLib.lookupFunction<csa_set_control_channel_func, csaSetControlChannelFunc>("csoundAndroidSetControlChannel");

typedef csa_get_control_channel_func = Double Function(Pointer<Void>, Pointer<Utf8>);
typedef csaGetControlChannelFunc = double Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidGetControlChannel = androLib.lookupFunction<csa_get_control_channel_func, csaGetControlChannelFunc>("csoundAndroidGetControlChannel");

typedef csa_compile_orc_func = Int32 Function(Pointer<Void>, Pointer<Utf8>);
typedef csaCompileOrcFunc = int Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidCompileOrc = androLib.lookupFunction<csa_compile_orc_func, csaCompileOrcFunc>("csoundAndroidCompileOrc");

typedef csa_compile_csd_func = Int32 Function(Pointer<Void>, Pointer<Utf8>);
typedef csaCompileCsdFunc = int Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidCompileCsd = androLib.lookupFunction<csa_compile_csd_func, csaCompileCsdFunc>("csoundAndroidCompileCsd");

typedef csa_read_score_func = Int32 Function(Pointer<Void>, Pointer<Utf8>);
typedef csaReadScoreFunc = int Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidReadScore = androLib.lookupFunction<csa_read_score_func, csaReadScoreFunc>("csoundAndroidReadScore");

typedef csa_get_audio_channel = Void Function(Pointer<Void>, Pointer<Utf8>, Pointer<Float>);
typedef csaGetAudioChannel = void Function(Pointer<Void>, Pointer<Utf8>, Pointer<Float>);
final csoundAndroidGetAudioChannel = androLib.lookupFunction<csa_get_audio_channel, csaGetAudioChannel>("csoundAndroidGetAudioChannel");

typedef csa_set_option = Int32 Function(Pointer<Void>, Pointer<Utf8>);
typedef csaSetOption = int Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidSetOption = androLib.lookupFunction<csa_set_option, csaSetOption>("csoundAndroidSetOption");

typedef csa_eval_code = Double Function(Pointer<Void>, Pointer<Utf8>);
typedef csaEvalCode = double Function(Pointer<Void>, Pointer<Utf8>);
final csoundAndroidEvalCode = androLib.lookupFunction<csa_eval_code, csaEvalCode>("csoundAndroidEvalCode");

final csoundAndroidCreatePointer = androLib.lookup<NativeFunction<csound_android_create_func>>("csoundAndroidCreate");
final csoundAndroidCreate = csoundAndroidCreatePointer.asFunction<csoundAndroidCreateFunc>();

final csoundAndroidCompileCsdtextPointer = androLib.lookup<NativeFunction<csound_android_compile_csd_text_func>>("csoundAndroidCompileCsdText");
final csoundAndroidCompileCsdText = csoundAndroidCompileCsdtextPointer.asFunction<csoundAndroidCompileCsdTextfunc>();

final csoundAndroidStartPointer = androLib.lookup<NativeFunction<csound_android_start_func>>("csoundAndroidStart");
final csoundAndroidStart = csoundAndroidStartPointer.asFunction<csoundAndroidStartFunc>();

final csoundAndroidStop = androLib.lookupFunction<csound_android_stop_func, csoundAndroidStopFunc>("csoundAndroidStop");

final printC = androLib.lookupFunction<Void Function(Pointer<Utf8>), void Function(Pointer<Utf8>)>("printC");

typedef csa_set_callback_port_func = Void Function(Pointer<Void>, Int64);
typedef csaSetCallbackPortFunc = void Function(Pointer<Void>, int);

final csoundAndroidSetCallbackPort = androLib.lookupFunction<csa_set_callback_port_func, csaSetCallbackPortFunc>("csoundAndroidSetCallbackPort");

typedef csa_pause = Void Function(Pointer<Void>, Int32);
typedef csaPause = void Function(Pointer<Void>, int);

final csoundAndroidPauseC = androLib.lookupFunction<csa_pause, csaPause>("csoundAndroidPause");

typedef csa_get_string_channel = Void Function(Pointer<Void>, Pointer<Utf8>, Pointer<Utf8>);
typedef csaGetStringChannel = void Function(Pointer<Void>, Pointer<Utf8>, Pointer<Utf8>);
final csoundAndroidGetStringChannelC = androLib.lookupFunction<csa_get_string_channel, csaGetStringChannel>("csoundAndroidGetStringChannel");

String csoundAndroidGetStringChannel(Pointer<Void> csound, Pointer<Utf8> channelName)
{
  Pointer<Utf8> ptr = calloc.allocate(128);
  csoundAndroidGetStringChannelC(csound, channelName, ptr);
  String str=  Utf8Pointer(ptr).toDartString();
  malloc.free(ptr);
  return str;
}


void csoundAndroidPause(Pointer<Void> cs, bool p)
{
  if(p) {
    csoundAndroidPauseC(cs, 1);
  } else {
    csoundAndroidPauseC(cs, 0);
  }
}

final nRegisterPostCObject = androLib.lookupFunction<
    Void Function(
        Pointer<NativeFunction<Int8 Function(Int64, Pointer<Dart_CObject>)>>
        functionPointer),
    void Function(
        Pointer<NativeFunction<Int8 Function(Int64, Pointer<Dart_CObject>)>>
        functionPointer)>('RegisterDart_PostCObject');


class CsoundAndroid implements Csound
{

  Map<String, CallbackAndroidType> _controlChannelCallbacks = {};
  Map<String, AudioCallbackAndroidType> _audioChannelCallbacks = {};
  ReceivePort _receivePortFromNative = ReceivePort();
  ReceivePort _receivePort = ReceivePort();
  ReceivePort _callbackPort = ReceivePort();
  late SendPort _sendPort;
  late Isolate _isolate;
  bool _isStarted = false;
  Function onReadyCallback = (){};
  Function onStopCallback = (){};
  Function onDestroyCallback = (){};


  CsoundAndroid({Function? onReady, Function? onStop})
  {
    if(onReady != null) {
      onReadyCallback = onReady;
    }
    if(onStop != null) {
      onStopCallback = onStop;
    }
    _startCsound();
  }

  void _startCsound()
  {
    nRegisterPostCObject(NativeApi.postCObject);
    _cs = csoundAndroidCreate();
    _startIsolate();
  }

  void dispose()
  {
    stop();
  }

  @override
  Future<int> compileCsd(String csdPath) {
    // TODO: implement compileCsd
    Pointer<Utf8> cName = StringUtf8Pointer(csdPath).toNativeUtf8();
    int res = csoundAndroidCompileCsd(_cs, cName);
    malloc.free(cName);
    return Future<int>.value(res);
  }

  @override
  Future<int> compileCsdText(String text) {
    Pointer<Utf8> cName = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundAndroidCompileCsdText(_cs, cName);
    malloc.free(cName);
    return Future<int>.value(res);
  }

  @override
  Future<int> compileOrc(String orc) {
    Pointer<Utf8> cName = StringUtf8Pointer(orc).toNativeUtf8();
    int res = csoundAndroidCompileOrc(_cs, cName);
    calloc.free(cName);
    return Future<int>.value(res);
  }

  @override
  Future<double> getControlChannel(String channelName) {
    Pointer<Utf8> cName = StringUtf8Pointer(channelName).toNativeUtf8();
    double value = csoundAndroidGetControlChannel(_cs, cName);
    malloc.free(cName);
    return Future<double>.value(value);
  }

  @override
  Future<String> getStringChannel(String channelName) {
    // TODO: implement getStringChannel
    throw UnimplementedError();
  }

  @override
  int perform() {
    _sendPort.send([{
      MessageTag.MESSAGE_TYPE: MessageType.PERFORM,
    }]);
    return 0;
  }

  @override
  Future<int> readScore(String text) {
    Pointer<Utf8> cName = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundAndroidReadScore(_cs, cName);
    malloc.free(cName);
    return Future<int>.value(res);
  }

  @override
  void setControlChannel(String channelName, double value) {
    Pointer<Utf8> cName = StringUtf8Pointer(channelName).toNativeUtf8();
    csoundAndroidSetControlChannel(_cs, StringUtf8Pointer(channelName).toNativeUtf8(), value);
    malloc.free(cName);
  }

  @override
  void setControlChannelCallback(String channelName, Function(double) callback) {
    if(_controlChannelCallbacks.containsKey(channelName) == false) {
      _controlChannelCallbacks[channelName] = CallbackAndroidType(StringUtf8Pointer(channelName).toNativeUtf8());
    }
    _controlChannelCallbacks[channelName]!.callbacks.add(callback);
    _sendMessage({MessageTag.MESSAGE_TYPE: MessageType.SET_AUDIO_CHANNEL_CALLBACK, MessageTag.CHANNEL_NAME: channelName});
  }

  @override
  void setStringChannel(String channelName, String value) {
    // TODO: implement setStringChannel
  }

  @override
  void setStringChannelCallback(String channelName, Function callback) {
    // TODO: implement setStringChannelCallback
  }

  @override
  void setTable(int table, List<double> values) {
    // TODO: implement setTable
  }

  @override
  void setTableCallback(int table, Function callback) {
    // TODO: implement setTableCallback
  }

  @override
  void stop() {

    if(_isStarted) {
      ReceivePort stopPort = ReceivePort();
      csoundAndroidStop(_cs);
      stopPort.listen((message) {
        _controlChannelCallbacks.forEach((key, value) {
          malloc.free(value.cName);
        });
        _audioChannelCallbacks.forEach((key, value) {
          if(value.cName != null && value.cName != nullptr) malloc.free(value.cName!);
        });
      });

      _sendPort.send([{MessageTag.MESSAGE_TYPE: MessageType.STOP}, stopPort.sendPort]);
    }

    _controlChannelCallbacks = {};
    _audioChannelCallbacks = {};
  }

  @override
  void pause(bool p) {
    csoundAndroidPause(_cs, p);
  }

  void setAudioChannelCallback(String name, Function f)
  {
    if(!_audioChannelCallbacks.containsKey(name)) {
      _audioChannelCallbacks[name] = AudioCallbackAndroidType.noBuffer();
    }
    _audioChannelCallbacks[name]!.callbacks.add(f);
    Map<String, String> msg = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_AUDIO_CHANNEL_CALLBACK, MessageTag.CHANNEL_NAME: name
    };
    _sendMessage(msg);
  }

  @override
  Future<int> getKsmps() {
    return Future<int>.value(csoundAndroidGetKsmps(_cs));
  }

  @override
  Future<int> getNchnls() {
    return Future<int>.value(csoundAndroidGetNchnls(_cs));
  }

  @override
  Future<double> getSr() {
    return Future<double>.value(csoundAndroidGetSr(_cs));
  }

  @override
  Future<double> evalCode(String code) {
    Pointer<Utf8> ptr = StringUtf8Pointer(code).toNativeUtf8();
    double res = csoundAndroidEvalCode(_cs, ptr);
    malloc.free(ptr);
    return Future<double>.value(res);
  }

  @override
  Future<double> get0dbfs() {
    return Future<double>.value(csoundAndroidGet0dBFS(_cs));
  }

  late Pointer<Void> _cs;

  @override
  Future<int> setOption(String opt) {
    Pointer<Utf8> ptr = StringUtf8Pointer(opt).toNativeUtf8();
    int res = csoundAndroidSetOption(_cs, ptr);
    malloc.free(ptr);
    return Future<int>.value(res);
  }

  void _sendMessage(Map<String, String> msg) async
  {
    _sendPort.send([msg]);
  }

  void _startIsolate() async {
    _receivePort = ReceivePort();
    ReceivePort exitPort = ReceivePort();
    exitPort.listen((message) {
    });
    this._isolate = await Isolate.spawn(
        CsoundAndroid._isolateEntryPoint, _receivePort.sendPort, onExit: exitPort.sendPort);
    _isStarted = true;
    _sendPort = await _receivePort.first;
    //Callback system init
    _callbackPort = ReceivePort();

    _sendPort.send([{
      MessageTag.MESSAGE_TYPE: MessageType.PASS_CSOUND_POINTER,
      MessageTag.MESSAGE_CONTENT: _cs.address.toString(),
    },
    ]);

    await this._initCallbackPort();

  }

  late SendPort nativeToIsolate;

  Future<void> _initCallbackPort() async {
    // Listen for audio and control callbacks
    _callbackPort.listen((message) async {
      String type = message[0][MessageTag.MESSAGE_TYPE];
      if(type == MessageType.SET_CONTROL_CHANNEL_CALLBACK) {
        double value = double.parse(message[0][MessageTag.CONTROL_CHANNEL_VALUE].toString());
        String channelName = message[0][MessageTag.CHANNEL_NAME];
        if(_controlChannelCallbacks.containsKey(channelName)) {
          _controlChannelCallbacks[channelName]!.callbacks.forEach((element) {
            element.call(value);
          });
        }
      } else if(type == MessageType.SET_AUDIO_CHANNEL_CALLBACK)
      {
        String channelName = message[0][MessageTag.CHANNEL_NAME];
        int ptr_adress = int.parse(message[0][MessageTag.AUDIO_CHANNEL_PTR]);
        if(_audioChannelCallbacks.containsKey(channelName)) {
          _audioChannelCallbacks[channelName]!.buf = ConcurrentBufferAndroid.fromAddress(ptr_adress);
          _audioChannelCallbacks[channelName]!.callbacks.forEach((element) {
            element.call(_audioChannelCallbacks[channelName]!.buf);
          });
        }
      } else if(type == MessageType.INIT_CALLBACK_PORT) { // Isolate returns the port native csound must call
        nativeToIsolate = message[1];
        //SendPort nativePort = message[1];
        csoundAndroidSetCallbackPort(_cs, nativeToIsolate.nativePort);
      } else if(type == MessageType.ISOLATE_READY) {
        onReadyCallback.call();
      }
    });

    _sendPort.send([
      {MessageTag.MESSAGE_TYPE: MessageType.INIT_CALLBACK_PORT},
      _callbackPort.sendPort
    ]);
  }
  static void _listenCallbacksFromNative(Pointer<Void> csound, Boolean isPlaying, Boolean pause, ReceivePort rPort, SendPort sendPort,
      Map<String, CallbackAndroidType> controlTasks, Map<String, AudioCallbackAndroidType> audioTasks) async
  {

    rPort = ReceivePort();
    rPort.listen((message) {
      //if(pause.value == true) return;
      controlTasks.forEach((key, value) {
        double val = csoundAndroidGetControlChannel(csound, value.cName);
        //just send callback with name and value
        sendPort.send([{
          MessageTag.MESSAGE_TYPE: MessageType.SET_CONTROL_CHANNEL_CALLBACK,
          MessageTag.CHANNEL_NAME: key,
          MessageTag.CONTROL_CHANNEL_VALUE: val.toString(),
        }]);
      });
      audioTasks.forEach((key, value) {
        csoundAndroidGetAudioChannel(csound, value.cName!, value.buf!.getBuffer());
        value.buf!.unlock();
        sendPort.send([{
          MessageTag.MESSAGE_TYPE: MessageType.SET_AUDIO_CHANNEL_CALLBACK,
          MessageTag.CHANNEL_NAME: key,
          MessageTag.AUDIO_CHANNEL_PTR: value.buf!.cBuf.address.toString()
        }]);
        //send callback with name and pointer to audiobuffer as int
      });

    });

    csoundAndroidSetCallbackPort(csound, rPort.sendPort.nativePort);
    int cnt = 0;

  }



  static void _isolateEntryPoint(SendPort sendPort) async {
    Map<String, CallbackAndroidType> controlChannelCallbacks = {};
    Map<String, AudioCallbackAndroidType> audioChannelCallbacks = {};

    Pointer<Void> csound = nullptr;
    ReceivePort receivePort = ReceivePort();
    sendPort.send(receivePort.sendPort);
    SendPort callbackPort = receivePort.sendPort; // just for init to avoid null
    Boolean isPlaying = Boolean(true);
    Boolean pause = Boolean(false);
    Boolean destroyBool = Boolean(false);
    ReceivePort receiveCallbackPort = ReceivePort();

    receivePort.listen((msg) {
      Map<String, String> map = msg[0];
      //SendPort reply = msg[1];
      String type = map[MessageTag.MESSAGE_TYPE]!;
        if(type == MessageType.PASS_CSOUND_POINTER) {
          csound = Pointer<Void>.fromAddress(int.parse(map[MessageTag.MESSAGE_CONTENT]!));
        } else if(type == MessageType.SET_AUDIO_CHANNEL_CALLBACK)
        {
          if(!audioChannelCallbacks.containsKey(map[MessageTag.CHANNEL_NAME])) {
            audioChannelCallbacks[map[MessageTag.CHANNEL_NAME]!] =
                AudioCallbackAndroidType(StringUtf8Pointer(map[MessageTag.CHANNEL_NAME]!).toNativeUtf8(), csoundAndroidGetKsmps(csound));
          }
        } else if(type == MessageType.SET_CONTROL_CHANNEL_CALLBACK)
        {
          if(!controlChannelCallbacks.containsKey(map[MessageTag.CHANNEL_NAME])) {
            controlChannelCallbacks[map[MessageTag.CHANNEL_NAME]!] =
                CallbackAndroidType(StringUtf8Pointer(map[MessageTag.CHANNEL_NAME]!).toNativeUtf8());
          }

        } else if(type == MessageType.INIT_CALLBACK_PORT)
        {
         callbackPort = msg[1];
         receiveCallbackPort = ReceivePort();
         csoundAndroidSetCallbackPort(csound, receiveCallbackPort.sendPort.nativePort);
         callbackPort.send([{MessageTag.MESSAGE_TYPE: MessageType.ISOLATE_READY}]);

        } else if(type == MessageType.PERFORM)
        {
          isPlaying.value = true;
          _listenCallbacksFromNative(csound, isPlaying, pause, receiveCallbackPort, callbackPort, controlChannelCallbacks, audioChannelCallbacks);
          csoundAndroidStart(csound);
        } else if(type == MessageType.STOP)
        {
          isPlaying.value = false;

          /*
          audioChannelCallbacks.forEach((key, value) {
            if(value.cName == nullptr) {
              print("audio cname null");
            }
            if(value.buf!.cBuf == nullptr) {
              print("audio buf null");
            }
            malloc.free(value.cName!);
            malloc.free(value.buf!.cBuf);
          });
          controlChannelCallbacks.forEach((key, value) {
            if(value.cName == nullptr) {
              print("control cname null");
            }
            malloc.free(value.cName);
          });
          */

          SendPort reply = msg[1];
          reply.send([0]);
        } else if(type == MessageType.PAUSE)
        {
          pause.value = (map[MessageTag.MESSAGE_CONTENT] == "true");
        } else if(type == MessageType.DELETE)
          {
            isPlaying.value = false;
            destroyBool.value = true;
            audioChannelCallbacks.forEach((key, value) {
              malloc.free(value.cName!);
              malloc.free(value.buf!.cBuf);
            });
            controlChannelCallbacks.forEach((key, value) {
              malloc.free(value.cName);
            });
            SendPort reply = msg[1];
            reply.send([0]);
          }
    });
    }

  @override
  int compileCsdSync(String csdPath) {
    Pointer<Utf8> ptr = StringUtf8Pointer(csdPath).toNativeUtf8();
    int res = csoundAndroidCompileCsd(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int compileCsdTextSync(String text) {
    Pointer<Utf8> ptr = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundAndroidCompileCsdText(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int compileOrcSync(String orc) {
    Pointer<Utf8> ptr = StringUtf8Pointer(orc).toNativeUtf8();
    int res = csoundAndroidCompileOrc(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  double evalCodeSync(String code) {
    Pointer<Utf8> ptr = StringUtf8Pointer(code).toNativeUtf8();
    double res = csoundAndroidEvalCode(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  double get0dbfsSync() {
    return csoundAndroidGet0dBFS(_cs);
  }

  @override
  double getControlChannelSync(String channelName) {
    Pointer<Utf8> ptr = StringUtf8Pointer(channelName).toNativeUtf8();
    double res = csoundAndroidGetControlChannel(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int getKsmpsSync() {
    return csoundAndroidGetKsmps(_cs);
  }

  @override
  int getNchnlsSync() {
    return csoundAndroidGetNchnls(_cs);
  }

  @override
  double getSrSync() {
    return csoundAndroidGetSr(_cs);
  }

  @override
  String getStringChannelSync(String channelName) {
    Pointer<Utf8> ptr = StringUtf8Pointer(channelName).toNativeUtf8();
    String res = csoundAndroidGetStringChannel(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int readScoreSync(String text) {
    Pointer<Utf8> ptr = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundAndroidReadScore(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int setOptionSync(String opt) {
    Pointer<Utf8> ptr = StringUtf8Pointer(opt).toNativeUtf8();
    int res = csoundAndroidSetOption(_cs, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  void destroy({Function? onDestroyed}) {
    if(onDestroyed != null) {
      onDestroyCallback = onDestroyed;
    }
    ReceivePort rPort = ReceivePort();
    rPort.listen((message) {
      csoundAndroidStop(_cs);
      csoundAndroidDestroy(_cs);
      _controlChannelCallbacks.forEach((key, value) {
        malloc.free(value.cName);
      });
      _audioChannelCallbacks.forEach((key, value) {
        if(value.cName != null && value.cName != nullptr) malloc.free(value.cName!);
      });
      _controlChannelCallbacks = {};
      _audioChannelCallbacks = {};
      _isolate.kill();
      onDestroyCallback.call();
    });
    _sendPort.send([{MessageTag.MESSAGE_TYPE: MessageType.DELETE}, rPort.sendPort]);
  }

  @override
  void reset() {
    csoundReset(_cs);
  }

  @override
  void resetSync() {
    csoundReset(_cs);
  }

}
