import 'dart:io';
import 'package:path/path.dart' as Path;

class CsoundPathFinder {
  static String findPath() {
    if (Platform.isLinux) {
      return getLinuxPath();
    } else if (Platform.isWindows) {
      return getWindowsPath();
    } else if (Platform.isMacOS) {
      return getMacOSPath();
    }
    throw ("Csound path not found");
  }

  static String getLinuxPath() {
    const String name = "libcsound64.so";

    for (int i = 0; i < linuxPaths.length; i++) {
      String path = Path.join(linuxPaths[i], name);
      if (File(path).existsSync()) {
        return path;
      }
    }

    throw ("Csound path not found");
  }

  static String getWindowsPath() {
    const String name = "csound64.dll";
    for (int i = 0; i < windowsPaths.length; i++) {
      final String path = Path.join(windowsPaths[i], name);
      if (File(path).existsSync()) {
        return path;
      }
    }
    throw ("Csound path not found");
  }

  static String getMacOSPath() {
    String path = "";
    for (int i = 0; i < macosPaths.length; i++) {
      if (Directory(macosPaths[i]).existsSync()) {
	return macosPaths[i];
      }
    }
    throw ("Csound path not found");
  }


  static const List<String> linuxPaths = [
    "/usr/lib",
    "/usr/lib/csound",
    "/usr/local/lib",
    "/usr/local/lib/csound",
    "/usr/lib/x64",
    "/usr/lib/x64/csound",
    "/usr/local/lib/x64",
    "/usr/local/lib/x64/csound",
  ];

  static const List<String> windowsPaths = [
    "C:/Program Files/Csound6_x64/bin",
    "C:/Program Files/Csound6_x64/lib",
    "C:/Program Files/Csound6_x64/csound",
  ];

  static const List<String> macosPaths = [
    "/Library/Frameworks/CsoundLib64.framework/CsoundLib64",
    "~/Library/Frameworks/CsoundLib64.framework/CsoundLib64",
    "/usr/local/Frameworks/CsoundLib64.framework/CsoundLib64",
  ];
}
