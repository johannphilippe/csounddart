import 'dart:ffi';
import 'package:csounddart/csound_dart_utilities.dart';
import 'package:csounddart/csound_android.dart';
import 'package:ffi/ffi.dart';
import 'dart:io';
import 'dart:isolate';
import 'package:csounddart/csound.dart';
import "package:csounddart/audio_utilities.dart";


import 'csound_path_finder.dart';

Future<void> requestPermissions() async {
  /*
  if(!Platform.isAndroid) return;
   Map<Permission, PermissionStatus> lst = await [Permission.storage,
     Permission.microphone, Permission.mediaLibrary, Permission.sensors].request();

   */
}

DynamicLibrary loadLib() {
  if (Platform.isAndroid) {
    DynamicLibrary lib = DynamicLibrary.open("libcsoundandroid.so");
    return lib;
  } 
  else if(Platform.isMacOS)
  {
    //DynamicLibrary lib = DynamicLibrary.open("CsoundLib64.framework");;
    DynamicLibrary lib = DynamicLibrary.process();
    return lib;
  }
  String libraryPath = CsoundPathFinder.findPath();
  DynamicLibrary lib = DynamicLibrary.open(libraryPath);
  return lib;
}

final lib = loadLib();

//Function signatures
// names are not really significant here (lowercase means C declaration, uppercase is Dart equivalent)

typedef voidFunc = Void Function();

typedef destroy_func = Void Function(Pointer<Void> csound);
typedef destroyFunc = void Function(Pointer<Void> csound);

typedef get_info_func = Double Function(Pointer<Void> csound);
typedef getInfoFunc = double Function(Pointer<Void> csound);

typedef get_uint_func = Int32 Function(Pointer<Void> csound);
typedef getUintFunc = int Function(Pointer<Void> csound);

typedef init_func = Int32 Function(Int32 flags);
typedef initFunc = int Function(int flags);

typedef create_func = Pointer<Void> Function(Pointer<Void> hostData);
typedef createFunc = Pointer<Void> Function(Pointer<Void> hostData);

typedef compile_csd_text_func = Int32 Function(
    Pointer<Void> csound, Pointer<Utf8> csdString);
typedef compileCsdTextFunc = int Function(
    Pointer<Void> csound, Pointer<Utf8> csdString);

typedef eval_code_func = Double Function(
    Pointer<Void> csound, Pointer<Utf8> string);
typedef evalCodeFunc = double Function(
    Pointer<Void> csound, Pointer<Utf8> string);

typedef perform_func = Int32 Function(Pointer<Void> csound);
typedef performFunc = int Function(Pointer<Void> csound);

typedef set_control_func = Void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Double value);
typedef setControlFunc = void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, double value);

typedef get_control_func = Double Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Int32> err);
typedef getControlFunc = double Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Int32> err);

typedef set_string_func = Void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Utf8> value);
typedef setStringFunc = void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Utf8> value);

typedef perf_routine_func = Int32 Function(Pointer<Void> data);
typedef create_thread_func = Pointer<Void> Function(
    Pointer<NativeFunction<perf_routine_func>> perf, Pointer<Void> data);

typedef get_table_func = Int32 Function(
    Pointer<Void> csound, Pointer<Pointer<Double>> tablePtr, Int32 tableNum);
typedef getTableFunc = int Function(
    Pointer<Void> csound, Pointer<Pointer<Double>> tablePtr, int tableNum);

typedef get_audio_channel_func = Void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Double> value);
typedef getAudioChannelFunc = void Function(
    Pointer<Void> csound, Pointer<Utf8> channelName, Pointer<Double> value);

typedef intTriggerFunc = Int32 Function(Pointer<Void> csoundInstance);

typedef set_debug_func = Void Function(Pointer<Void> csound, Int32 debug);
typedef setDebugFunc = void Function(Pointer<Void> csound, int debug);

typedef get_first_message_func = Pointer<Utf8> Function(Pointer<Void> csound);
typedef getFirstMessageFunc = Pointer<Utf8> Function(Pointer<Void> csound);

typedef get_channel_ptr_func = Int32 Function(Pointer<Void> csound,
    Pointer<Pointer<Double>> ptr, Pointer<Utf8> channelName, Int32 type);
typedef getChannelPtrFunc = int Function(Pointer<Void> csound,
    Pointer<Pointer<Double>> ptr, Pointer<Utf8> channelName, int type);



// Csound's C API functions wrapping.

final csoundInitializePointer =
    lib.lookup<NativeFunction<init_func>>("csoundInitialize");
final csoundInitialize = csoundInitializePointer.asFunction<initFunc>();

final csoundCreatePointer =
    lib.lookup<NativeFunction<create_func>>("csoundCreate");
final csoundCreate = csoundCreatePointer.asFunction<createFunc>();

final csoundCompileCsdTextPointer =
    lib.lookup<NativeFunction<compile_csd_text_func>>("csoundCompileCsdText");
final csoundCompileCsdText =
    csoundCompileCsdTextPointer.asFunction<compileCsdTextFunc>();

final csoundPerformPointer =
    lib.lookup<NativeFunction<perform_func>>("csoundPerform");
final csoundPerform = csoundPerformPointer.asFunction<performFunc>();

final csoundPerformKsmpsPointer =
    lib.lookup<NativeFunction<perform_func>>("csoundPerformKsmps");
final csoundPerformKsmps = csoundPerformKsmpsPointer.asFunction<performFunc>();

final csoundStartPointer =
    lib.lookup<NativeFunction<perform_func>>("csoundStart");
final csoundStart = csoundStartPointer.asFunction<performFunc>();

final csoundDestroyPointer =
    lib.lookup<NativeFunction<destroy_func>>("csoundDestroy");
final csoundDestroy = csoundDestroyPointer.asFunction<destroyFunc>();

final csoundCompileOrcPointer =
    lib.lookup<NativeFunction<compile_csd_text_func>>("csoundCompileOrc");
final csoundCompileOrc =
    csoundCompileOrcPointer.asFunction<compileCsdTextFunc>();

final csoundReadScorePointer =
    lib.lookup<NativeFunction<compile_csd_text_func>>("csoundReadScore");
final csoundReadScore = csoundReadScorePointer.asFunction<compileCsdTextFunc>();

final csoundEvalCodePointer =
    lib.lookup<NativeFunction<eval_code_func>>("csoundEvalCode");
final csoundEvalCode = csoundEvalCodePointer.asFunction<evalCodeFunc>();

final csoundCompilePointer =
    lib.lookup<NativeFunction<compile_csd_text_func>>("csoundCompile");
final csoundCompile = csoundCompilePointer.asFunction<compileCsdTextFunc>();

final csoundStopPointer =
    lib.lookup<NativeFunction<destroy_func>>("csoundStop");
final csoundStop = csoundStopPointer.asFunction<destroyFunc>();

final csoundCleanupPointer =
    lib.lookup<NativeFunction<destroy_func>>("csoundStop");
final csoundCleanup = csoundCleanupPointer.asFunction<destroyFunc>();

final csoundResetPointer =
    lib.lookup<NativeFunction<destroy_func>>("csoundReset");
final csoundReset = csoundResetPointer.asFunction<destroyFunc>();

final csoundGetSrPointer =
    lib.lookup<NativeFunction<get_info_func>>("csoundGetSr");
final csoundGetSr = csoundGetSrPointer.asFunction<getInfoFunc>();

final csoundGet0dBFSPointer =
    lib.lookup<NativeFunction<get_info_func>>("csoundGet0dBFS");
final csoundGet0dBFS = csoundGet0dBFSPointer.asFunction<getInfoFunc>();

final csoundGetKsmpsPointer =
    lib.lookup<NativeFunction<get_uint_func>>("csoundGetKsmps");
final csoundGetKsmps = csoundGetKsmpsPointer.asFunction<getUintFunc>();

final csoundGetNchnlsPointer =
    lib.lookup<NativeFunction<get_uint_func>>("csoundGetNchnls");
final csoundGetNchnls = csoundGetNchnlsPointer.asFunction<getUintFunc>();

final csoundGetNchnlsIsInputPointer =
    lib.lookup<NativeFunction<get_uint_func>>("csoundGetNchnlsIsInput");
final csoundGetNchnlsIsInput =
    csoundGetNchnlsIsInputPointer.asFunction<getUintFunc>();

final csoundSetOptionPointer =
    lib.lookup<NativeFunction<compile_csd_text_func>>("csoundSetOption");
final csoundSetOption = csoundSetOptionPointer.asFunction<compileCsdTextFunc>();

final csoundSetControlChannelPointer =
    lib.lookup<NativeFunction<set_control_func>>("csoundSetControlChannel");
final csoundSetControlChannel =
    csoundSetControlChannelPointer.asFunction<setControlFunc>();

final csoundGetControlChannelPointer =
    lib.lookup<NativeFunction<get_control_func>>("csoundGetControlChannel");
final csoundGetControlChannel =
    csoundGetControlChannelPointer.asFunction<getControlFunc>();

final csoundSetStringChannelPointer =
    lib.lookup<NativeFunction<set_string_func>>("csoundSetStringChannel");
final csoundSetStringChannel =
    csoundSetStringChannelPointer.asFunction<setStringFunc>();

final csoundGetStringChannelPointer =
    lib.lookup<NativeFunction<set_string_func>>("csoundGetStringChannel");
final csoundGetStringChannelC =
    csoundGetStringChannelPointer.asFunction<setStringFunc>();

final csoundCreateThreadPointer =
    lib.lookup<NativeFunction<create_thread_func>>("csoundCreateThread");
final csoundCreateThread =
    csoundCreateThreadPointer.asFunction<create_thread_func>();

final csoundGetTablePointer =
    lib.lookup<NativeFunction<get_table_func>>("csoundGetTable");
final csoundGetTableC = csoundGetTablePointer.asFunction<getTableFunc>();

final csoundGetAudioChannelPointer =
    lib.lookup<NativeFunction<get_audio_channel_func>>("csoundGetAudioChannel");
final csoundGetAudioChannel =
    csoundGetAudioChannelPointer.asFunction<getAudioChannelFunc>();

final csoundSetDebugPointer =
    lib.lookup<NativeFunction<set_debug_func>>("csoundSetDebug");
final csoundSetDebug = csoundSetDebugPointer.asFunction<setDebugFunc>();

final csoundCreateMessageBufferPointer =
    lib.lookup<NativeFunction<set_debug_func>>("csoundCreateMessageBuffer");
final csoundCreateMessageBuffer =
    csoundCreateMessageBufferPointer.asFunction<setDebugFunc>();

final csoundGetMessageCntPointer =
    lib.lookup<NativeFunction<get_uint_func>>("csoundGetMessageCnt");
final csoundGetMessageCnt =
    csoundGetMessageCntPointer.asFunction<getUintFunc>();

final csoundGetFirstMessagePointer =
    lib.lookup<NativeFunction<get_first_message_func>>("csoundGetFirstMessage");
final csoundGetFirstMessage =
    csoundGetFirstMessagePointer.asFunction<getFirstMessageFunc>();

final csoundPopFirstMessagePointer =
    lib.lookup<NativeFunction<destroy_func>>("csoundPopFirstMessage");
final csoundPopFirstMessage =
    csoundPopFirstMessagePointer.asFunction<destroyFunc>();

final csoundGetChannelPtrPointer =
    lib.lookup<NativeFunction<get_channel_ptr_func>>("csoundGetChannelPtr");
final csoundGetChannelPtr =
    csoundGetChannelPtrPointer.asFunction<getChannelPtrFunc>();

final csoundCompileCsd = lib.lookupFunction<compile_csd_text_func, compileCsdTextFunc>("csoundCompileCsd");

//wrapper function (string utf8 conversion)
String csoundGetStringChannel(Pointer<Void> csound, Pointer<Utf8> cName) {
  Pointer<Utf8> ptr = calloc.allocate(128);
  csoundGetStringChannelC(
      csound, cName, ptr);
  String str=  Utf8Pointer(ptr).toDartString();
  malloc.free(ptr);
  return str;
}

List<double> csoundGetTable(Pointer<Void> csound, int table) {
  Pointer<Pointer<Double>> ptr = Pointer.fromAddress(nullptr.address);
  int res = csoundGetTableC(csound, ptr, table);
  Pointer<Double> myfltPtr = ptr[0];
  List<double> l = List.generate(res, (index) => 0.0);
  for (int i = 0; i < res; i++) l[i] = myfltPtr[i];
  return l;
}

void csoundSetTable(Pointer<Void> csound, List<double> value, int table) {
  Pointer<Pointer<Double>> ptr = Pointer.fromAddress(nullptr.address);
  int res = csoundGetTableC(csound, ptr, table);
  for (int i = 0; i < res; i++) ptr[0][i] = value[i];
}

// Messages tags and types for communication with the Isolate
class MessageTag {
  static const String MESSAGE_TYPE = "1";
  static const String MESSAGE_CONTENT = "2";
  static const String CONTROL_CHANNEL_VALUE = "3";
  static const String STRING_CHANNEL_VALUE = "4";
  static const String CHANNEL_NAME = "5";
  static const String CALLBACK_INDEX = "6";
  static const String TABLE_INDEX = "7";
  static const String AUDIO_CHANNEL_DATA = "8";
  static const String AUDIO_CHANNEL_PTR = "9";

  static const String TYPE_CONTROL = "10";
  static const String TYPE_AUDIO = "11";

  static const String CSOUND_POINTER = "12";
}

class MessageType {
  static const String COMPILE_CSD_TEXT = "1";
  static const String COMPILE_CSD = "2";
  static const String COMPILE_ORC = "3";
  static const String READ_SCORE = "4";
  static const String PERFORM = "5";
  static const String STOP = "6";
  static const String SET_CONTROL_CHANNEL = "7";
  static const String GET_CONTROL_CHANNEL = "8";
  static const String DELETE = "9";
  static const String SET_CONTROL_CHANNEL_CALLBACK = "10";

  static const String SET_STRING_CHANNEL = "11";
  static const String GET_STRING_CHANNEL = "12";
  static const String SET_STRING_CHANNEL_CALLBACK = "13";
  static const String SET_TABLE = "14";
  static const String SET_TABLE_CALLBACK = "15";

  static const String SET_AUDIO_CHANNEL_CALLBACK = "16";

  static const String GET_KSMPS = "17";
  static const String GET_SR = "18";
  static const String GET_NCHNLS = "19";
  static const String EVAL_CODE = "20";
  static const String GET_0DBFS = "21";
  static const String SET_OPTION = "22";
  static const String PAUSE = "23";
  static const String RESET = "24";

  static const String ISOLATE_READY = "98";
  static const String PASS_CSOUND_POINTER = "99";
  static const String INIT_CALLBACK_PORT = "0";
  static const String CUSTOM_MESSAGE = "-1";
}

Map<String, String> generateMessage(String messageType, {String content = ""}) {
  Map<String, String> map = new Map<String, String>();
  map[MessageTag.MESSAGE_TYPE] = messageType.toString();
  map[MessageTag.MESSAGE_CONTENT] = content;
  return map;
}

Map<String, String> generateControlMessage(String channelName, double value) {
  Map<String, String> map = new Map<String, String>();
  map[MessageTag.MESSAGE_TYPE] = MessageType.SET_CONTROL_CHANNEL;
  map[MessageTag.CHANNEL_NAME] = channelName;
  map[MessageTag.CONTROL_CHANNEL_VALUE] = value.toString();
  return map;
}

/*
This class is the current implementation of Csound for desktop platform. It might be used for the implementation of mobile platforms as well.
When creating the object, a new isolate (thread) is created. Starting from that, every call to Csound methods sends a message to the isolate, which will
perform the triggered action.

A more synchronous implementation was experimented with CsoundSmart (below in the file)
 */


class CsoundNative implements Csound {

  bool _isolateRunning = false;
  late Pointer<Void> _csound;
  Function onReadyCallback = (){};
  Function onDestroyCallback = (){};
  Function onStopCallback = (){};

  CsoundNative({Function? onReady, Function? onStop}) {
    if(onReady != null) {
      onReadyCallback = onReady;
    }
    if(onStop != null) {
      onStopCallback = onStop;
    }
    _startCsound();
  }

  void _startCsound()
  {
    _callbackPort = ReceivePort();
    _receivePort = ReceivePort();
    _csound = csoundCreate(nullptr);
    _startIsolate();
  }

  Future<int> compileCsdText(String text) async {
    //_sendMessage(generateMessage(MessageType.COMPILE_CSD_TEXT, content: text));
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.COMPILE_CSD_TEXT,
      MessageTag.MESSAGE_CONTENT: text,
    };
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);
    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  Future<int> compileOrc(String orc) async {
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.COMPILE_ORC,
      MessageTag.MESSAGE_CONTENT: orc,
    };
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);
    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  Future<int> compileCsd(String csd) async {
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.COMPILE_CSD,
      MessageTag.MESSAGE_CONTENT: csd,
    };
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);
    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  Future<int> readScore(String score) async {
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.READ_SCORE,
      MessageTag.MESSAGE_CONTENT: score,
    };

    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  void setControlChannel(String channelName, double value) {
    _sendMessage(generateControlMessage(channelName, value));
  }

  Future<double> getControlChannel(String channelName) async {
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.GET_CONTROL_CHANNEL,
      MessageTag.CHANNEL_NAME: channelName
    };
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    double res = 0.0;

    await for (var msg in rport) {
      res = double.parse(msg[0][MessageTag.CONTROL_CHANNEL_VALUE]);
      break;
    }
    rport.close();

    return res;
  }

  int perform() {
    //start performance
    _sendMessage(generateMessage(MessageType.PERFORM));
    //start callback listening
    return 0;
  }

  void stop() {
    _sendMessage(generateMessage(MessageType.STOP));
  }

  void setControlChannelCallback(String channelName, Function callback) {
    Map<String, String> map =
        generateMessage(MessageType.SET_CONTROL_CHANNEL_CALLBACK, content: "");
    map[MessageTag.CHANNEL_NAME] = channelName;
    _callbackList.add(callback);
    map[MessageTag.CALLBACK_INDEX] = _callbackList.length.toString();
    _sendPort.send([map, _callbackPort.sendPort]);
  }

  void setAudioChannelCallback(String channel, Function f) {
    Map<String, String> map =
        generateMessage(MessageType.SET_AUDIO_CHANNEL_CALLBACK, content: "");
    map[MessageTag.CHANNEL_NAME] = channel;
    _callbackList.add(f);
    map[MessageTag.CALLBACK_INDEX] = _callbackList.length.toString();
    _sendPort.send([map, _callbackPort.sendPort]);
  }

  void setStringChannel(String channelName, String value) {
    Map<String, String> map = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_STRING_CHANNEL,
      MessageTag.CHANNEL_NAME: channelName,
      MessageTag.STRING_CHANNEL_VALUE: value,
    };
    ReceivePort rport = ReceivePort();
    _sendPort.send([map, rport.sendPort]);
  }

  Future<String> getStringChannel(String channelName) async {
    //First make getControlChannel work, then implement this one
    String s = "";
    return s;
  }

  void setStringChannelCallback(String channelName, Function callback) {
    _callbackList.add(callback);
    Map<String, String> map = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_STRING_CHANNEL_CALLBACK,
      MessageTag.CHANNEL_NAME: channelName,
      MessageTag.CALLBACK_INDEX: _callbackList.length.toString(),
    };
    _sendPort.send([map, _callbackPort.sendPort]);
  }

  void setTable(int table, List<double> value) {
    Map<String, String> map = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_TABLE,
      MessageTag.TABLE_INDEX: table.toString(),
    };
    _sendPort.send([map, _callbackPort.sendPort, value]);
  }

  void setTableCallback(int table, Function callback) {
    _callbackList.add(callback);
    Map<String, String> map = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_TABLE_CALLBACK,
      MessageTag.TABLE_INDEX: table.toString(),
      MessageTag.CALLBACK_INDEX: _callbackList.length.toString(),
    };
    _sendPort.send([map, _callbackPort.sendPort]);
  }

  @override
  Future<int> getKsmps() async {
    Map<String, String> m = {MessageTag.MESSAGE_TYPE: MessageType.GET_KSMPS};
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  Future<int> getNchnls() async {
    Map<String, String> m = {MessageTag.MESSAGE_TYPE: MessageType.GET_NCHNLS};
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  Future<double> getSr() async {
    Map<String, String> m = {MessageTag.MESSAGE_TYPE: MessageType.GET_SR};
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    double res = 0;
    await for (var msg in rport) {
      res = double.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  Future<double> get0dbfs() async {
    Map<String, String> m = {MessageTag.MESSAGE_TYPE: MessageType.GET_0DBFS};
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);

    double res = 0;
    await for (var msg in rport) {
      res = double.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  Future<int> setOption(String opt) async {
    Map<String, String> m = {
      MessageTag.MESSAGE_TYPE: MessageType.SET_OPTION,
      MessageTag.MESSAGE_CONTENT: opt,
    };
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort]);
    int res = 0;
    await for (var msg in rport) {
      res = int.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  Future<double> evalCode(String code) async {
    Map<String, String> m = {MessageTag.MESSAGE_TYPE: MessageType.EVAL_CODE};
    ReceivePort rport = new ReceivePort();
    _sendPort.send([m, rport.sendPort, code]);
    double res = 0;
    await for (var msg in rport) {
      res = double.parse(msg[0][MessageTag.MESSAGE_CONTENT]);
      break;
    }
    rport.close();
    return res;
  }

  @override
  void pause(bool b) {
    _sendPort.send([{MessageTag.MESSAGE_TYPE: MessageType.PAUSE, MessageTag.MESSAGE_CONTENT: b.toString()}, _receivePort.sendPort]);
  }


  // ISOLATE METHODS (Mostly private threaded implementation)
  void _sendMessage(Map<String, String> msg) {
    //_isolateChannel.sink.add(msg);
    ReceivePort rport = ReceivePort();
    _sendPort.send([msg, rport.sendPort]);
  }

  void _startIsolate() async {
    _receivePort = ReceivePort();
    ReceivePort exitPort = ReceivePort();

    exitPort.listen((message) {
    });
    _isolate = await Isolate.spawn(
        CsoundNative._isolateEntryPoint, _receivePort.sendPort, onExit: exitPort.sendPort);
    _isolateRunning = true;
    _sendPort = await _receivePort.first;
    //Callback system init
    _callbackList = [];
    _callbackPort = ReceivePort();

    _sendPort.send([{
      MessageTag.MESSAGE_TYPE: MessageType.PASS_CSOUND_POINTER,
      MessageTag.MESSAGE_CONTENT: _csound.address.toString(),
    }, _callbackPort.sendPort
    ]);
    await this._initCallbackPort();
  }

  Future<void> _initCallbackPort() async {
    // Listen for callback control messages
    _callbackPort.listen((message)  {
      if(message[1] == MessageType.DELETE) {
        _isolateRunning = false;
        _isolate.kill();
        onDestroyCallback.call();
      }
      if(!_isolateRunning) {
        return;
      }
      if(message[1] == MessageType.STOP) {
        onStopCallback.call();
      }
      if (message[1] == MessageTag.TYPE_CONTROL) {
        _callbackList[message[0] - 1].call(message[2]);
      } else if (message[1] == MessageTag.TYPE_AUDIO) {
        ConcurrentBuffer buf = ConcurrentBuffer.fromAddress(message[2]);
        _callbackList[message[0] - 1].call(buf);
      } else if(message[1] == MessageType.ISOLATE_READY) {
        onReadyCallback.call();
      }
    });

    _sendPort.send([
      {MessageTag.MESSAGE_TYPE: MessageType.INIT_CALLBACK_PORT},
      _callbackPort.sendPort
    ]);
  }

  static void _perfAsync(
      Pointer<Void> csound,
      Map<String, CallbackType> controlTasks,
      Map<String, CallbackType> stringTasks,
      Map<String, CallbackType> tableTasks,
      Map<String, CallbackType> audioTasks,
      Map<String, ConcurrentBuffer> audioChannels,
      SendPort callbackPort,
      Boolean keepPlaying,
      Boolean pause,
      Boolean destroyBool,
      ) async {

    keepPlaying.value = true;
    while (csoundPerformKsmps(csound) == 0 && keepPlaying.value == true) {
      while(pause.value == true) {
        await Future.delayed(Duration(milliseconds: 50));
      }
      await Future(() {});
      controlTasks.forEach((String key, CallbackType channel) {
        double val = csoundGetControlChannel(
            csound, channel.cName, nullptr);
        channel.list.forEach((int element) {
          // each element is index of the correspondant callback
          callbackPort.send(
              [element, MessageTag.TYPE_CONTROL, val]); // send index, and value
        });
      });
      stringTasks.forEach((String key, CallbackType channel) {
        String val = csoundGetStringChannel(csound, channel.cName);
        channel.list.forEach((int element) {
          callbackPort.send([element, MessageTag.TYPE_CONTROL, val]);
        });
      });
      tableTasks.forEach((key, channel) {
        List<double> val = csoundGetTable(csound, int.parse(key));
        channel.list.forEach((element) {
          callbackPort.send([element, MessageTag.TYPE_CONTROL, val]);
        });
      });
      audioTasks.forEach((String key, CallbackType channel) {
        csoundGetAudioChannel(csound, channel.cName,
            audioChannels[key]!.getBuffer());
        audioChannels[key]!.unlock();
        channel.list.forEach((int element) {
          callbackPort.send([
            element,
            MessageTag.TYPE_AUDIO,
            audioChannels[key]!.cBuf.address
          ]);
        });
        // To do --> add a layer for csoundGetAudioChannel (convert ptr to List<double>)
      });
    }

    controlTasks.forEach((key, value) {
      malloc.free(value.cName);
    });
    controlTasks = {};

    stringTasks.forEach((key, value) {
      malloc.free(value.cName);
    });
    stringTasks = {};

    tableTasks.forEach((key, value) {
      malloc.free(value.cName);
    });

    tableTasks = {};
    audioTasks.forEach((key, value) {
      malloc.free(value.cName);
    });

    audioChannels.forEach((key, value) {
      value.destroy();
    });
    audioChannels = {};
    audioTasks = {};

    //csoundDestroy(csound);

    if(destroyBool.value == true ) {
      csoundDestroy(csound);
      callbackPort.send([0, MessageType.DELETE]);
    } else {
      callbackPort.send([0, MessageType.STOP]);
    }
  }

  static void _isolateEntryPoint(SendPort sendPort) async {
    // functions index in main isolate
    Map<String, CallbackType> isolateAudioChannelTasks = {};
    //Map<String, Pointer<Double>> audioChannelData = {};
    Map<String, ConcurrentBuffer> audioChannelBuffers = {};

    Map<String, CallbackType> isolateControlChannelTasks = {};
    Map<String, CallbackType> isolateStringChannelTasks = {};
    Map<String, CallbackType> isolateTableTasks = {};
    Boolean keepPlaying = Boolean(false);
    Boolean pause = Boolean(false);
    Boolean destroyBool = Boolean(false);

    Pointer<Void> csound =  nullptr; //csoundCreate(nullptr);
    ReceivePort receivePort = ReceivePort();
    sendPort.send(receivePort.sendPort);
    SendPort callbackPort = receivePort.sendPort; // just for init to avoid null


    receivePort.listen((msg) {
      Map<String, String> map = msg[0];
      SendPort reply = msg[1];

      String type = map[MessageTag.MESSAGE_TYPE]!;

      if (type == MessageType.COMPILE_CSD_TEXT) {
        Pointer<Utf8> cName = StringUtf8Pointer(map[MessageTag.MESSAGE_CONTENT]!).toNativeUtf8();
        int res = csoundCompileCsdText(csound,
            cName);
        malloc.free(cName);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.COMPILE_CSD_TEXT,
          MessageTag.MESSAGE_CONTENT: res.toString()
        };
        reply.send([m]);
      } else if (type == MessageType.COMPILE_CSD) {
        //csoundCompile(csound, Utf8.toUtf8(map[MessageTag.MESSAGE_CONTENT]));
        Pointer<Utf8> cName = StringUtf8Pointer(
            map[MessageTag.MESSAGE_CONTENT]!).toNativeUtf8();
        int res = csoundCompileCsdText(csound,
            cName);
        malloc.free(cName);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.COMPILE_CSD,
          MessageTag.MESSAGE_CONTENT: res.toString()
        };
        reply.send([m]);
      } else if (type == MessageType.PASS_CSOUND_POINTER) {
        csound = Pointer<Void>.fromAddress(int.parse(map[MessageTag.MESSAGE_CONTENT]!));
      } else if (type == MessageType.COMPILE_ORC) {
        Pointer<Utf8> cName = StringUtf8Pointer(map[MessageTag.MESSAGE_CONTENT]!).toNativeUtf8();
        int res = csoundCompileOrc(csound,
            cName);
        malloc.free(cName);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.COMPILE_ORC,
          MessageTag.MESSAGE_CONTENT: res.toString()
        };
        reply.send([m]);
      } else if (type == MessageType.READ_SCORE) {
        Pointer<Utf8> cName = StringUtf8Pointer(map[MessageTag.MESSAGE_CONTENT]!).toNativeUtf8();
        int res = csoundReadScore(csound,
            cName);
        malloc.free(cName);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.READ_SCORE,
          MessageTag.MESSAGE_CONTENT: res.toString()
        };
        reply.send([m]);
      } else if (type == MessageType.PERFORM) {
        csoundStart(csound);
        CsoundNative._perfAsync(
            csound,
            isolateControlChannelTasks,
            isolateStringChannelTasks,
            isolateTableTasks,
            isolateAudioChannelTasks,
            audioChannelBuffers,
            callbackPort,
            keepPlaying, pause, destroyBool);
      } else if (type == MessageType.STOP) {
        keepPlaying.value = false;
        csoundStop(csound);

      } else if (type == MessageType.SET_CONTROL_CHANNEL) {
        Pointer<Utf8> cName = StringUtf8Pointer(map[MessageTag.CHANNEL_NAME]!).toNativeUtf8();
        csoundSetControlChannel(
            csound,
            cName,
            double.parse(map[MessageTag.CONTROL_CHANNEL_VALUE]!));
        malloc.free(cName);

      } else if (type == MessageType.GET_CONTROL_CHANNEL) {
        Pointer<Utf8> cName = StringUtf8Pointer(map[MessageTag.CHANNEL_NAME]!).toNativeUtf8();
        double val = csoundGetControlChannel(
            csound,
            cName,
            nullptr);
        malloc.free(cName);
        Map<String, String> m = Map<String, String>();
        m[MessageTag.MESSAGE_TYPE] = MessageType.GET_CONTROL_CHANNEL;
        m[MessageTag.CONTROL_CHANNEL_VALUE] = val.toString();
        reply.send(m);

      } else if (type == MessageType.GET_KSMPS) {
        int ksmps = csoundGetKsmps(csound);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.GET_KSMPS,
          MessageTag.MESSAGE_CONTENT: ksmps.toString()
        };
        reply.send([m]);

      } else if (type == MessageType.GET_NCHNLS) {
        int nchnls = csoundGetNchnls(csound);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.GET_KSMPS,
          MessageTag.MESSAGE_CONTENT: nchnls.toString()
        };
        reply.send([m]);

      } else if (type == MessageType.GET_SR) {
        double sr = csoundGetSr(csound);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.GET_KSMPS,
          MessageTag.MESSAGE_CONTENT: sr.toString()
        };
        reply.send([m]);

      } else if (type == MessageType.GET_0DBFS) {
        double sr = csoundGet0dBFS(csound);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.GET_0DBFS,
          MessageTag.MESSAGE_CONTENT: sr.toString()
        };
        reply.send([m]);

      } else if (type == MessageType.DELETE) {
        if(keepPlaying.value == false) {
          csoundDestroy(csound);
          callbackPort.send([0, MessageType.DELETE]);
        }
        destroyBool.value = true;
        keepPlaying.value = false;

        //csoundDestroy(csound);

      } else if (type == MessageType.SET_CONTROL_CHANNEL_CALLBACK) {
        if (callbackPort == null) callbackPort = reply;
        if (!isolateControlChannelTasks
            .containsKey(map[MessageTag.CHANNEL_NAME]))
          isolateControlChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list = [];
        isolateControlChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list
            .add(int.parse(map[MessageTag.CALLBACK_INDEX]!));

      } else if (type == MessageType.SET_AUDIO_CHANNEL_CALLBACK) {
        if (callbackPort == null) callbackPort = reply;
        if (!isolateAudioChannelTasks.containsKey(map[MessageTag.CHANNEL_NAME]))
          {
            isolateAudioChannelTasks[map[MessageTag.CHANNEL_NAME]!] = CallbackType(StringUtf8Pointer(map[MessageTag.CHANNEL_NAME]!).toNativeUtf8());
            isolateAudioChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list = [];
          }

        int ksmps = csoundGetKsmps(csound);
        isolateAudioChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list
            .add(int.parse(map[MessageTag.CALLBACK_INDEX]!));
        //audioChannelData[map[MessageTag.CHANNEL_NAME]!] = calloc.allocate(sizeOf<Double>() * ksmps);
        audioChannelBuffers[map[MessageTag.CHANNEL_NAME]!] =
            ConcurrentBuffer(ksmps);

      } else if (type == MessageType.SET_STRING_CHANNEL_CALLBACK) {
        if (callbackPort == null) callbackPort = reply;
        if (!isolateStringChannelTasks
            .containsKey(map[MessageTag.CHANNEL_NAME]))
          isolateStringChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list = [];
        isolateStringChannelTasks[map[MessageTag.CHANNEL_NAME]!]!.list
            .add(int.parse(map[MessageTag.CALLBACK_INDEX]!));

      } else if (type == MessageType.INIT_CALLBACK_PORT) {
        callbackPort = reply;
        reply.send([0, MessageType.ISOLATE_READY]);

      } else if (type == MessageType.SET_TABLE) {
        List<double> l = msg[2];
        csoundSetTable(csound, l, int.parse(map[MessageTag.TABLE_INDEX]!));

      } else if (type == MessageType.SET_TABLE_CALLBACK) {
        if (callbackPort == null) callbackPort = reply;
        if (!isolateTableTasks.containsKey(map[MessageTag.TABLE_INDEX]))
          isolateTableTasks[map[MessageTag.TABLE_INDEX]!]!.list = [];
        isolateTableTasks[map[MessageTag.TABLE_INDEX]!]!.list
            .add(int.parse(map[MessageTag.CALLBACK_INDEX]!));

      } else if (type == MessageType.EVAL_CODE) {
        Pointer<Utf8> cName = StringUtf8Pointer(msg[2]).toNativeUtf8();
        double res = csoundEvalCode(csound, cName);
        malloc.free(cName);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.GET_KSMPS,
          MessageTag.MESSAGE_CONTENT: res.toString()
        };
        reply.send([m]);

      } else if(type == MessageType.SET_OPTION)
      {
        Pointer<Utf8> opt = StringUtf8Pointer(msg[MessageTag.MESSAGE_CONTENT]).toNativeUtf8();
        int res = csoundSetOption(csound, opt);
        malloc.free(opt);
        Map<String, String> m = {
          MessageTag.MESSAGE_TYPE: MessageType.SET_OPTION,
          MessageTag.MESSAGE_CONTENT: res.toString(),
        };
        reply.send([m]);
      } else if(type == MessageType.PAUSE)
      {
        bool p = (map[MessageTag.MESSAGE_CONTENT]! == "true");
        pause.value = p;

      } else if(type == MessageType.RESET)
        {
          csoundReset(csound);
        }
    });
  }

  ReceivePort _callbackPort = ReceivePort();
  late List<Function> _callbackList;
  late Isolate _isolate;
  ReceivePort _receivePort = ReceivePort();
  late SendPort _sendPort;

  @override
  int compileCsdSync(String csdPath) {
    Pointer<Utf8> ptr = StringUtf8Pointer(csdPath).toNativeUtf8();
    int res = csoundCompileCsd(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int compileCsdTextSync(String text) {
    Pointer<Utf8> ptr = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundCompileCsdText(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int compileOrcSync(String orc) {
    Pointer<Utf8> ptr = StringUtf8Pointer(orc).toNativeUtf8();
    int res = csoundCompileOrc(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  double evalCodeSync(String code) {
   Pointer<Utf8> ptr = StringUtf8Pointer(code).toNativeUtf8();
   double res = csoundEvalCode(_csound, ptr);
   malloc.free(ptr);
   return res;
  }

  @override
  double get0dbfsSync() {
    return csoundGet0dBFS(_csound);
  }

  @override
  double getControlChannelSync(String channelName) {
    Pointer<Utf8> ptr = StringUtf8Pointer(channelName).toNativeUtf8();
    double res = csoundGetControlChannel(_csound, ptr, nullptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int getKsmpsSync() {
    return csoundGetKsmps(_csound);
  }

  @override
  int getNchnlsSync() {
    return csoundGetNchnls(_csound);
  }

  @override
  double getSrSync() {
   return csoundGetSr(_csound); 
  }

  @override
  String getStringChannelSync(String channelName) {
    Pointer<Utf8> ptr = StringUtf8Pointer(channelName).toNativeUtf8();
    String res = csoundGetStringChannel(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int readScoreSync(String text) {
    Pointer<Utf8> ptr = StringUtf8Pointer(text).toNativeUtf8();
    int res = csoundReadScore(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  int setOptionSync(String opt) {
    Pointer<Utf8> ptr = StringUtf8Pointer(opt).toNativeUtf8();
    int res = csoundSetOption(_csound, ptr);
    malloc.free(ptr);
    return res;
  }

  @override
  void destroy({Function? onDestroyed}) {
    if(onDestroyed != null) {
      onDestroyCallback = onDestroyed;
    }
    _sendPort.send([{
      MessageTag.MESSAGE_TYPE: MessageType.DELETE,
    }, _callbackPort.sendPort]);
  }

  @override
  void reset() {
    _sendPort.send([{MessageTag.MESSAGE_TYPE: MessageType.RESET}, _callbackPort.sendPort]);
  }

  @override
  void resetSync()
  {
    csoundReset(_csound);
  }

}

/*
Static methods, and selector of implementation
 */


Csound getCsoundDart({Function? onReady, Function? onStop}) {
  if (Platform.isAndroid) {
    requestPermissions();
    return CsoundAndroid(onReady: onReady, onStop: onStop);
  }
  return CsoundNative(onReady: onReady, onStop: onStop);
}

Function getAppStartup() {
  return () {
    requestPermissions();
  };
}

double csoundEvaluateCode(String code)
{
  double res = -1;
  Pointer<Utf8> ptr = StringUtf8Pointer(code).toNativeUtf8();
  if(Platform.isAndroid) {
    Pointer<Void> csound = csoundAndroidCreate();
    res = csoundAndroidEvalCode(csound, ptr);
  } else {
    Pointer<Void> csound = csoundCreate(nullptr);
    res = csoundEvalCode(csound, ptr);
  }
  malloc.free(ptr);
  return res;
}



























