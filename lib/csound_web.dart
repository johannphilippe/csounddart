@JS()
library csound;

import 'csound.dart';
import 'package:js/js.dart';
import 'package:js/js_util.dart' show promiseToFuture;
import 'dart:html' as html;

Future<void> importLibs() async
{
}


/* Removed in Csound 6.15
@JS("importScripts")
external dynamic importScripts(String s);


Future importJsScripts(String s) {
  return promiseToFuture(importScripts(s));
}
*/

/*
JS Wrappers around CsoundObj and CsoundDart.js (a glue code file linked in the plugin).
 */
@JS("resumeAudioState")
external void resumeAudioState();
@JS("suspendAudioState")
external void suspendAudioState();

@JS("csoundInitialize")
external dynamic csoundInitialize();

Future initializeCsound() async {
  //return promiseToFuture(csoundInitialize());
}

@JS("CsoundJsDart")
class CsoundJsDart
{
  // Constructor
 external CsoundJsDart();

 external initialize();
  // start and play both start engine and play performance
 external start();
 external play();
 external stop();
 external reset();

  //set option string to Csound
 external setOption(String optionString);

 external compileCSD(String csdString);
 external compileOrc(String orcString);
 external readScore(String score);

 // not fully working now
 external evaluateCode(String codeString);


 external setControlChannel(String channelName, double value);
 external setStringChannel(String channelName, String stringValue);


 external num getControlChannel(String channelName);
 external String getStringChannel(String channelName);

  // callback to perform when channel is available
 external requestControlChannel(String channelName, Function callback);
 external requestStringChannel(String channelName, Function callback);

 external setMessageCallback(Function msgCallback);

  // receives a callback function taking a number as argument (channel value)
  // need to use allowInterop to pass callback function
 external setControlChannelCallbackJs(String channelName, Function callback);
 external setStringChannelCallbackJs(String channelName, Function callback);


 external setTable(int table, List<double> value);
 external setTableCallbackJs(int table, Function callback);
}

@JS("createCsound")
external CsoundJsDart createCsound();

/*
The implementation of Csound for web.
 */
class CsoundWeb implements Csound
{

  Function onReadyCallback = (){};
  Function onStopCallback = (){};

  CsoundWeb({Function? onReady, Function? onStop}) {
    if(onReady != null) {
      onReadyCallback = onReady;
    }
    if(onStop != null) {
      onStopCallback = onStop;
    }

    initializeCsound().then((value) {
      //resumeAudioState();
      //_csound = CsoundJsDart();
      _csound = CsoundJsDart();
      //_csound = createCsound();
      onReadyCallback.call();
    });

  }

  Future<int> compileCsdText(String text)
  {
    _csound.compileCSD(text);
    return Future<int>.value(0);
  }

  Future<int> compileOrc(String orc)
  {
    _csound.compileOrc(orc);
    return Future<int>.value(0);
  }

  Future<int> compileCsd(String csd)
  {
    _csound.compileCSD(csd);
    return Future<int>.value(0);
  }

  Future<int> readScore(String score)
  {
    _csound.readScore(score);
    return Future<int>.value(0);
  }

  int perform()
  {
    _csound.start();
    return 0;
  }

  void pause(bool b)
  {
    throw(UnimplementedError());
  }

// Web implementation of Csound (CsoundObj) seems threaded by default, to verify

  void stop()
  {
    _csound.stop();
  }

  void setControlChannel(String channel, double val)
  {
    _csound.setControlChannel(channel, val);
  }

  Future<double> getControlChannel(String channel) async
  {
    return _csound.getControlChannel(channel).toDouble();
  }

  void setControlChannelCallback(String channelName, Function callback)
  {
    _csound.setControlChannelCallbackJs(channelName, callback);
  }

  void setStringChannel(String channelName, String value)
  {
    _csound.setStringChannel(channelName, value);
  }
  
  Future<String> getStringChannel(String channelName) async 
  {
    return _csound.getStringChannel(channelName);
  }

  void setStringChannelCallback(String channelName, Function callback) 
  {
    _csound.setStringChannelCallbackJs(channelName, callback);
  }


  void setTable(int table, List<double> value)
  {
    _csound.setTable(table, value); 
  }

  void setTableCallback(int table, Function callback)
  {
    _csound.setTableCallbackJs(table, callback);
  }

  void setAudioChannelCallback(String channel, Function f)
  {
    throw UnimplementedError("Not implemented for web");
  }
  //member


  late CsoundJsDart _csound;

  @override
  Future<int> getKsmps() {
    // TODO: implement getKsmps
    throw UnimplementedError("Not impleted for web");
  }

  @override
  Future<int> getNchnls() {
    // TODO: implement getNchnls
    throw UnimplementedError("Not impleted for web");
  }

  @override
  Future<double> getSr() {
    // TODO: implement getSr
    throw UnimplementedError("Not impleted for web");
  }

  @override
  Future<double> evalCode(String code) {
    // TODO: implement evalCode
    throw UnimplementedError();
  }

  @override
  Future<double> get0dbfs() {
    // TODO: implement get0dbfs
    throw UnimplementedError();
  }

  Future<int> setOption(String opt)
  {
    throw UnimplementedError();
  }

  @override
  int compileCsdSync(String csdPath) {
    // TODO: implement compileCsdSync
    throw UnimplementedError();
  }

  @override
  int compileCsdTextSync(String text) {
    // TODO: implement compileCsdTextSync
    throw UnimplementedError();
  }

  @override
  int compileOrcSync(String orc) {
    // TODO: implement compileOrcSync
    throw UnimplementedError();
  }

  @override
  double evalCodeSync(String code) {
    // TODO: implement evalCodeSync
    throw UnimplementedError();
  }

  @override
  double get0dbfsSync() {
    // TODO: implement get0dbfsSync
    throw UnimplementedError();
  }

  @override
  double getControlChannelSync(String channelName) {
    // TODO: implement getControlChannelSync
    throw UnimplementedError();
  }

  @override
  int getKsmpsSync() {
    // TODO: implement getKsmpsSync
    throw UnimplementedError();
  }

  @override
  int getNchnlsSync() {
    // TODO: implement getNchnlsSync
    throw UnimplementedError();
  }

  @override
  double getSrSync() {
    // TODO: implement getSrSync
    throw UnimplementedError();
  }

  @override
  String getStringChannelSync(String channelName) {
    // TODO: implement getStringChannelSync
    throw UnimplementedError();
  }

  @override
  int readScoreSync(String text) {
    // TODO: implement readScoreSync
    throw UnimplementedError();
  }

  @override
  int setOptionSync(String opt) {
    // TODO: implement setOptionSync
    throw UnimplementedError();
  }

  @override
  void destroy({Function? onDestroyed}) {
    // TODO: implement destroy
  }

  @override
  void reset() {
    // TODO: implement reset
  }

  @override
  void resetSync() {
    // TODO: implement resetSync
  }
}

Csound getCsoundDart({Function? onReady, Function? onStop}) {
  importLibs();
  return CsoundWeb(onReady: onReady, onStop: onStop);
}

Function getAppStartup()
{
  return () {};
}

double csoundEvaluateCode(String code)
{
  throw UnimplementedError();
}