import 'dart:ffi';
import 'package:path/path.dart' as Path;
import 'dart:io';

/*
Path resolution must be improved to work on all platforms (including raspberry, where bundling is different, and not native build from flutter, so must be done by hand)
 */
DynamicLibrary loadAudioUtilities() {
  if (Platform.isLinux) {
    String libPath = Path.dirname(Platform.resolvedExecutable);
    String path = Path.join(libPath, "lib", "libaudio_utilities.so");
    if (!File(path).existsSync()) {
      path = "/usr/lib/libaudio_utilities.so";
    }
    print("final executable path : $path");

    DynamicLibrary aLib =
        DynamicLibrary.open(Path.join(libPath, "lib", "libaudio_utilities.so"));

    return aLib;
  } else if (Platform.isWindows) {
    DynamicLibrary aLib = DynamicLibrary.open(Path.join(
        Path.dirname(Platform.resolvedExecutable), "csounddart_plugin.dll"));
    return aLib;
  } else if(Platform.isAndroid) {
    DynamicLibrary aLib = DynamicLibrary.open("libaudio_utilities.so");
    return aLib;
  } else if(Platform.isMacOS)
  {
    DynamicLibrary aLib = DynamicLibrary.process();
    return aLib;
  }

  throw ("Could not load audio_utilities");
}

final aLib = loadAudioUtilities();

typedef create_buf_func = Pointer<Void> Function(Int32 size);
typedef createBufFunc = Pointer<Void> Function(int size);

typedef destroy_buf_func = Void Function(Pointer<Void> buf);
typedef destroyBufFunc = void Function(Pointer<Void> buf);

typedef buffer_set_func = Void Function(
    Pointer<Void> buf, Int32 index, Double val);
typedef bufferSetFunc = void Function(Pointer<Void> buf, int index, double val);

typedef buffer_get_func = Double Function(Pointer<Void> buf, Int32 index);
typedef bufferGetFunc = double Function(Pointer<Void> buf, int index);

typedef buffer_get_buffer_func = Pointer<Double> Function(Pointer<Void> buf);
typedef bufferGetBufferFunc = Pointer<Double> Function(Pointer<Void> buf);

typedef buffer_unlock_func = Void Function(Pointer<Void> buf);
typedef bufferUnlockFunc = void Function(Pointer<Void> buf);

typedef buffer_get_size_func = Int32 Function(Pointer<Void> buf);
typedef bufferGetSizeFunc = int Function(Pointer<Void> buf);

final bufferCreatePointer =
    aLib.lookup<NativeFunction<create_buf_func>>("create_concurrent_buffer");
final bufferCreate = bufferCreatePointer.asFunction<createBufFunc>();

final bufferDestroyPointer =
    aLib.lookup<NativeFunction<destroy_buf_func>>("destroy_concurrent_buffer");
final bufferDestroy = bufferDestroyPointer.asFunction<destroyBufFunc>();

final bufferSetPointer =
    aLib.lookup<NativeFunction<buffer_set_func>>("buffer_set");
final bufferSet = bufferSetPointer.asFunction<bufferSetFunc>();

final bufferGetPointer =
    aLib.lookup<NativeFunction<buffer_get_func>>("buffer_get_value");
final bufferGet = bufferGetPointer.asFunction<bufferGetFunc>();

final bufferGetBufferPointer =
    aLib.lookup<NativeFunction<buffer_get_buffer_func>>("buffer_get");
final bufferGetBuffer =
    bufferGetBufferPointer.asFunction<bufferGetBufferFunc>();

final bufferUnlockPointer =
    aLib.lookup<NativeFunction<buffer_unlock_func>>("buffer_unlock");
final bufferUnlock = bufferUnlockPointer.asFunction<bufferUnlockFunc>();

final bufferGetSizePointer =
    aLib.lookup<NativeFunction<buffer_get_size_func>>("buffer_get_size");
final bufferGetSize = bufferGetSizePointer.asFunction<bufferGetSizeFunc>();

class ConcurrentBuffer {
  late Pointer<Void> cBuf;
  int _length = 0;

  ConcurrentBuffer(int size) {
    this.cBuf = bufferCreate(size);
    _length = size;
  }

  ConcurrentBuffer.fromPointer(Pointer<Void> ptr) {
    cBuf = ptr;
    _length = bufferGetSize(cBuf);
  }

  ConcurrentBuffer.fromAddress(int ptr) {
    cBuf = Pointer<Void>.fromAddress(ptr);
    _length = bufferGetSize(cBuf);
  }

  int get length {
    return _length;
  }

  void set(int index, double value) {
    bufferSet(cBuf, index, value);
  }

  double get(int index) {
    return bufferGet(cBuf, index);
  }

  Pointer<Double> getBuffer() {
    return bufferGetBuffer(cBuf);
  }

  void unlock() {
    bufferUnlock(cBuf);
  }

  void destroy() {
    bufferDestroy(cBuf);
  }
}



typedef buffer_set_func_android = Void Function(
    Pointer<Void> buf, Int32 index, Float val);
typedef bufferSetFuncAndroid = void Function(Pointer<Void> buf, int index, double val);

typedef buffer_get_func_android = Float Function(Pointer<Void> buf, Int32 index);
typedef bufferGetFuncAndroid = double Function(Pointer<Void> buf, int index);

typedef buffer_get_buffer_func_android = Pointer<Float> Function(Pointer<Void> buf);
typedef bufferGetBufferFuncAndroid = Pointer<Float> Function(Pointer<Void> buf);

final bufferSetPointerAndroid =
aLib.lookup<NativeFunction<buffer_set_func_android>>("buffer_set");
final bufferSetAndroid = bufferSetPointerAndroid.asFunction<bufferSetFuncAndroid>();

final bufferGetPointerAndroid =
aLib.lookup<NativeFunction<buffer_get_func_android>>("buffer_get_value");
final bufferGetAndroid = bufferGetPointerAndroid.asFunction<bufferGetFuncAndroid>();

final bufferGetBufferPointerAndroid =
aLib.lookup<NativeFunction<buffer_get_buffer_func_android>>("buffer_get");
final bufferGetBufferAndroid =
bufferGetBufferPointerAndroid.asFunction<bufferGetBufferFuncAndroid>();



class ConcurrentBufferAndroid {
  late Pointer<Void> cBuf;
  int _length = 0;

  ConcurrentBufferAndroid(int size) {
    this.cBuf = bufferCreate(size);
    _length = size;
  }

  ConcurrentBufferAndroid.fromPointer(Pointer<Void> ptr) {
    cBuf = ptr;
    _length = bufferGetSize(cBuf);
  }

  ConcurrentBufferAndroid.fromAddress(int ptr) {
    cBuf = Pointer<Void>.fromAddress(ptr);
    _length = bufferGetSize(cBuf);
  }

  int get length {
    return _length;
  }

  void set(int index, double value) {
    bufferSetAndroid(cBuf, index, value);
  }

  double get(int index) {
    return bufferGetAndroid(cBuf, index);
  }

  Pointer<Float> getBuffer() {
    return bufferGetBufferAndroid(cBuf);
  }

  void unlock() {
    bufferUnlock(cBuf);
  }

  void destroy() {
    bufferDestroy(cBuf);
  }
}
