import "csound.dart";

Csound getCsoundDart({Function? onReady, Function? onStop}) => throw UnsupportedError("Cannot find a working implementation of Csound for this platform");
Function getAppStartup() => throw UnsupportedError("App startup method not found");
double csoundEvaluateCode(String code) => throw UnsupportedError("Eval Code static method is not implemented for this platform");
