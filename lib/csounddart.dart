
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:csounddart/csound.dart';

class Csounddart {
  static const MethodChannel _channel =
      const MethodChannel('csounddart');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
