#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint csounddart.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'csounddart'
  s.version          = '0.0.1'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Johann Philippe' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files     = 'Classes/**/*'
  s.dependency 'FlutterMacOS'
  s.frameworks = 'CsoundLib64'
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => ['/usr/local/Frameworks', '/Library/Frameworks', "#{ ENV['HOME'] }/Library/Frameworks"] }

  s.platform = :osx, '10.11'
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }
  s.swift_version = '5.0'
end
