//
// Created by johann on 05/08/21.
//

#include<iostream>
#include<memory.h>
#include<mutex>
#include<stdlib.h>
#include<condition_variable>

extern "C"
{

class ConcurrentBuffer
{
public:
    ConcurrentBuffer(size_t sizeOfBuffer) : size(sizeOfBuffer) {
        buffer = (double *)::calloc(size, sizeof(double));
    }

    ~ConcurrentBuffer()
    {
        //mutex_.unlock();
        //delete[] buffer;
    }
    
    void set(int index, double v) {
        std::lock_guard<std::mutex> guard(mutex_);
        if(index >= size) return;
        //std::unique_lock<std::mutex> lock(mutex_);
        //mutex_.lock();
        buffer[index] = v;
        //lock.unlock();
        //mutex_.unlock();
        //condition_variable_.notify_one();
    }

    double get(int index)
    {
        std::lock_guard<std::mutex> guard(mutex_);

        if(index >= size) {
            throw("index out of bounds");
        //return -1;
        }
        //std::unique_lock<std::mutex> lock(mutex_);
        //mutex_.lock();
        double value = buffer[index];
        //lock.unlock();
        //condition_variable_.notify_one();
        //mutex_.unlock();
        return value;
    }

    double *getBuffer()
    {
        std::lock_guard<std::mutex> guard(mutex_);

        //std::unique_lock<std::mutex> lock_ = std::unique_lock<std::mutex>(mutex_);
        //mutex_.lock();
        return buffer;
    }

    void unlock()
    {
        //mutex_.unlock();
       //lock_.unlock();
       // condition_variable_.notify_one();
    }

    void free()
    {
        //mutex_.lock();
        //std::unique_lock<std::mutex> lock(mutex_);
        delete[] buffer;
        //mutex_.unlock();
        //mutex_.unlock();
        //lock.unlock();
        //condition_variable_.notify_one();
    }


    //std::unique_lock<std::mutex> lock_;

    //std::condition_variable condition_variable_;
    size_t size;
    double *buffer;
    std::mutex mutex_;
};

__attribute__((visibility("default"))) __attribute__((used))
ConcurrentBuffer * create_concurrent_buffer(int size)
{
    return new ConcurrentBuffer(size);
}

__attribute__((visibility("default"))) __attribute__((used))
void buffer_set(ConcurrentBuffer *buf, int index, double value)
{
    buf->set(index, value);
}

__attribute__((visibility("default"))) __attribute__((used))
double buffer_get_value(ConcurrentBuffer *buf, int index)
{
    return buf->get(index);
}

__attribute__((visibility("default"))) __attribute__((used))
double *buffer_get(ConcurrentBuffer *buf)
{
    return buf->getBuffer();
}

__attribute__((visibility("default"))) __attribute__((used))
void buffer_unlock(ConcurrentBuffer *buf)
{
    buf->unlock();
}

__attribute__((visibility("default"))) __attribute__((used))
void destroy_concurrent_buffer(ConcurrentBuffer *buf)
{
    buf->free();
    delete buf;
}

__attribute__((visibility("default"))) __attribute__((used))
int buffer_get_size(ConcurrentBuffer *buf)
{
    return (int)buf->size;
}

}
