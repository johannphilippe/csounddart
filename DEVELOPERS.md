# Developers guide

## Implementation Notes

### API

The unified API can be found in `csound.dart` file. It is an abstract class that enables implementation
for different targets. It is designed to perform concurrently, in order not to freeze the interface.
This API tries to be as close as possible to Csound's C++ API. But, since it targets multiple platforms,
I had to make a few decisions in order to allow using the same interface for Web and Native platforms.
Thus, part of the design is implemented in a web fashion (channel callbacks for example).

### Desktop and Android

Desktop and Android implementations can be found in `csound_native.dart` and `csound_android.dart`.
When creating an object by calling `Csound()` constructor, it immediatly creates a Csound instance
calling `csoundCreate`, and creates an Isolate (Dart equivalent for Thread) and shares the Csound pointer.
The reason why it was made this way is that Isolates can be quite slow to launch,
depending on platforms.
I decided to implement it this way in order to prevent a delay when calling `perform` method.
Some of the methods are async and also have synchronous equivalents.
On the native side (desktop), the async method will send a message to the worker isolate which will perform
the action, and send a message back to the listener port. The reason of this weird implementation is that
I spent a lot of time understanding how to share the Csound pointer between Isolates. The synchronous
methods are faster, since they directly perform the action. The second reason for async methods is that
I currently don't know if there are synchronous equivalents in the Web implementation,
based on `CsoundObj.js`.

### Web

Built upon `CsoundObj.js`, it is a quite direct layer. Only a bit of glue code is written in `CsoundDart.js`.
A few methods available on native and Android implementation aren't implemented yet (audio callbacks, some getters and setters).
